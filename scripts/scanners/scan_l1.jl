using DataFrames;
using CSV;

include(string(@__DIR__) * "/../semi_analytical_scanner.jl")

Λs = 10 .^ range(-7, stop=0, length=100);
Ns = 10 .^ range(log10(5), stop=log10(200), length=100);
L1=[1e-3, 2.0];
c= 1.0;
μη=1.0;
μΔ=1.0;
ξinf = 1e-1;

df= scan_sensitivity(Λs, Ns, L1, c, μη, μΔ, ξinf, [true, false]);
CSV.write("/Users/loganmorrison/.julia/dev/DarkSUN/data/l1.csv", df);
