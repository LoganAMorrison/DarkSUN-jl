using ProgressMeter;
using DataFrames;
using CSV;
using Interpolations;
using Roots
using ForwardDiff
using Calculus
using QuadGK

include("../src/thermodynamic_particles.jl")
include("../src/standard_model.jl")
include("../src/thermal_cs.jl")
include("../src/compute_xi.jl")
include("../src/constants.jl")
include("../src/utils.jl")

mutable struct DarkSUNModel
    # Parameters
    Λ::Float64
    N::Float64
    L1::Float64
    c::Float64
    μη::Float64
    μΔ::Float64
    ξinf::Float64
    η::ThermodynamicBoson
    Δ::ThermodynamicFermion
    has_dp::Bool
    # freeze out
    has_frozen::Bool
    ξfo::Float64
    Tsmfo::Float64
    # Initializations
    x_init::Float64
    x_final::Float64
    Td_init::Float64
    Tsm_init::Float64
end

function DarkSUNModel(Λ::Float64, N::Float64, L1::Float64, c::Float64, μη::Float64, μΔ::Float64, ξinf::Float64, has_dp::Bool)
    η = ThermodynamicBoson(μη * Λ / √N, 1.0)
    Δ = ThermodynamicFermion(μΔ * Λ * N, 2.0)
    # Compute initial variables
    Td0::Float64 = η.mass
    hdinf::Float64 = dark_dof_entropy_inf(N, has_dp)
    hd::Float64 = dark_dof_entropy(Td0, η, Δ, has_dp)
    ξ0::Float64 = compute_ξ_const_Td(Td0, hd, hdinf, ξinf)
    Tsm0::Float64 = Td0 / ξ0
    xinit::Float64 = η.mass / Tsm0
    winit = [log(η.neq(Td0) / sm_entropy_density(Tsm0))]
    xfinal = η.mass / T_CMB

    return DarkSUNModel(Λ, N, L1, c, μη, μΔ, ξinf, η, Δ, has_dp, false,
                        NaN, NaN, xinit, xfinal, Td0, Tsm0)
end

@inline function dark_dof_entropy_inf(N::Float64, has_dp::Bool)
    7.0 / 2.0 * N + 2N^2 - 2.0 + (has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy_inf(model::DarkSUNModel)
    dark_dof_entropy_inf(model.N, model.has_dp)
end

@inline function sum_internal_dark_dof(m::DarkSUNModel)
    m.η.g + 7.0 / 8.0 * m.Δ.g + (m.has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy(Td::Float64, η::ThermodynamicBoson, Δ::ThermodynamicFermion, has_dp::Bool)
    η.dof_entropy(Td) + Δ.dof_entropy(Td) + (has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy(Td::U, m::DarkSUNModel) where U<:Real
    m.η.dof_entropy(Td) + m.Δ.dof_entropy(Td) + (m.has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_energy(Td::U, m::DarkSUNModel) where U<:Real
    m.η.dof_energy(Td) + m.Δ.dof_energy(Td) + (m.has_dp ? 2.0 : 0.0)
end

function thermal_cs_ηη_ηηηη(Td::U, model::DarkSUNModel) where U<:Real
    fη::Float64 = sqrt(model.N) * model.Λ / (4π)
    pf::Float64 = (model.L1^2 * model.η.mass^7 / (fη * model.Λ)^4)^2
    pf * thermal_cs_ηη_ηηηη(model.η.mass / Td, model.N)
end

function thermal_cs_ηη_ΔΔ(Td::U, model::DarkSUNModel) where U<:Real
    cs = thermal_cs_ηη_ΔΔ(Td, model.N, model.Λ, model.c, model.μΔ, model.μη)
    isfinite(cs) ? cs : 0.0
end

function compute_ξ(Tsm::U, model::DarkSUNModel) where U<:Real
    darkh(Td::U) where U<:Real = dark_dof_entropy(Td, model)
    hdinf::Float64 = dark_dof_entropy_inf(model)
    sumg::Float64 = sum_internal_dark_dof(model)
    gl::Float64 = model.has_dp ? 2.0 : model.η.g
    ml::Float64 = model.η.mass
    ξinf::Float64 = model.ξinf
    if model.has_dp
        return compute_ξ_const_Tsm(Tsm, darkh, hdinf, sumg, gl, ξinf)
        #return compute_ξ_const_Tsm(Tsm, model.ξcurrent, darkh, hdinf, ξinf)
    else
        if model.has_frozen
            # If the particle freezes out when it it relativistic, ξ=ξfo
            if model.Tsmfo * model.ξfo > model.η.mass
                return model.ξfo
            # If the particle freezes out when it it non-relativistic,
            # ξ is redshifted
            else
                return model.ξfo * Tsm / model.Tsmfo
            end
        else # no dark photon and hasn't frozen out
            return compute_ξ_const_Tsm(Tsm, darkh, hdinf, sumg, gl, ml, ξinf)
            #return compute_ξ_const_Tsm(Tsm, model.ξcurrent, darkh, hdinf, ξinf)
        end
    end
end

function Yeq(x, model::DarkSUNModel)
    T = model.η.mass / x
    ξ = compute_ξ(T, model)
    model.η.neq(ξ * T) / sm_entropy_density(T)
end

function xstar_eqn(xstar, model::DarkSUNModel)
    mη = model.η.mass
    T = mη / xstar
    ξ = compute_ξ(T, model)
    Td = T * ξ
    # yeq stuff
    yeq = Yeq(xstar, model)
    dyeq = Calculus.derivative(x -> Yeq(x, model), xstar)
    # Compute gstar
    _dark_dof_energy = dark_dof_energy(Td, model)
    _sm_dof_energy = sm_dof_energy(T)
    _dof_energy_eff = _sm_dof_energy + _dark_dof_energy * ξ^4
    sqrt_gstar = sm_sqrt_gstar(T) * sqrt(_sm_dof_energy / _dof_energy_eff)
    # ⟨σv⟩
    σηη = thermal_cs_ηη_ηηηη(Td, model)

    dyeq + yeq^2 * σηη * sqrt(π/45) * sqrt_gstar * M_PLANK * mη / xstar^2
end

function find_xstar_max(model::DarkSUNModel; maxiter = 100)
    xstarmax = 10.0
    iter = 0
    while true
        val = xstar_eqn(xstarmax, model)
        if val == 0.0
            xstarmax *= 0.9
        elseif val < 0.0
            return xstarmax
        else
            xstarmax *= 1.1
        end
        iter+=1
        if iter > maxiter
            throw("cant find xstar_max")
        end
    end
end

function find_xstar_min(model::DarkSUNModel; maxiter = 100)
    xstarmax = 0.1
    iter = 0
    while true
        val = xstar_eqn(xstarmax, model)
        if val <= 0.0
            xstarmax *= 0.9
        else
            return xstarmax
        end
        iter+=1
        if iter > maxiter
            throw("cant find xstar_max")
        end
    end
end

"""
    compute_xstar(model::DarkSUNModel)

Compute the value of x such that the η begins to freeze.
"""
function compute_xstar!(model::DarkSUNModel)
    xmax = find_xstar_max(model)
    xmin = find_xstar_min(model)
    f(x) = xstar_eqn(x, model)
    xstar = find_zero(f, (xmin, xmax), Bisection())
    # Set freeze-out variables
    model.Tsmfo = model.η.mass / xstar
    model.ξfo = compute_ξ(model.Tsmfo, model)
    model.has_frozen = true

    xstar
end

"""
    alpha_integrand(x, model::DarkSUNModel)

Compute the integrand for integral used to compute α.
"""
function alpha_integrand(x, model::DarkSUNModel)
    mη = model.η.mass
    T = mη / x
    ξ = compute_ξ(T, model)
    Td = T * ξ
    # yeq stuff
    yeq = Yeq(x, model)
    # Compute gstar
    _dark_dof_energy = dark_dof_energy(Td, model)
    _sm_dof_energy = sm_dof_energy(T)
    _dof_energy_eff = _sm_dof_energy + _dark_dof_energy * ξ^4
    sqrt_gstar = sm_sqrt_gstar(T) * sqrt(_sm_dof_energy / _dof_energy_eff)
    # ⟨σv⟩
    σηη = thermal_cs_ηη_ηηηη(Td, model)

    res = sqrt_gstar * σηη / (x * yeq)^2
    isfinite(res) ? res : 0.0
end

"""
    integrand_Δ(x, model::DarkSUNModel)

Compute the integrand for integral used to compute YΔ(∞)
"""
function integrand_Δ(x, model::DarkSUNModel)
    mη = model.η.mass
    T = mη / x
    ξ = compute_ξ(T, model)
    Td = T * ξ
    # yeq stuff
    yeq = Yeq(x, model)
    # Compute gstar
    _dark_dof_energy = dark_dof_energy(Td, model)
    _sm_dof_energy = sm_dof_energy(T)
    _dof_energy_eff = _sm_dof_energy + _dark_dof_energy * ξ^4
    sqrt_gstar = sm_sqrt_gstar(T) * sqrt(_sm_dof_energy / _dof_energy_eff)
    σ = thermal_cs_ηη_ΔΔ(Td, model.N, model.Λ, model.c, model.μΔ, model.μη)

    σ * sqrt_gstar * yeq^2 / x^2
end

"""
    compute_Yfs(model::DarkSUNModel)

Compute the relic comoving-number densities of the η and Δ given the `model`.
"""
function compute_Yfs(model::DarkSUNModel)
    xstar = compute_xstar!(model)
    pf = model.η.mass * M_PLANK * sqrt(π/45)

    α = pf * quadgk(x->alpha_integrand(x,model), xstar, Inf)[1]

    Ys = Yeq(xstar, model)
    Yη = Ys / cbrt(1 + 3α * Ys^3)
    model.has_frozen = false
    YΔ = pf * quadgk(x->integrand_Δ(x, model), model.x_init, xstar)[1]
    model.has_frozen = true
    (Yη, YΔ)
end

"""
    compute_rds(model::DarkSUNModel)

Compute the relic densities of the η and Δ given the `model`.
"""
function compute_rds(model::DarkSUNModel)
    Yfs = compute_Yfs(model)
    r = S_TODAY / RHO_CRIT
    (model.η.mass * Yfs[1] * r, model.Δ.mass * Yfs[2] * r)
end

"""
    cs_ηη_ηη(model)

Compute the self-interaction cross section of the η.
"""
function cs_ηη_ηη(model::DarkSUNModel)
    # compute dark temperature today:
    Tsm_today = 2.7255 * 8.6173303e-14
    Td_today = compute_ξ(Tsm_today, model) * Tsm_today

    mη = model.η.mass;
    vrel = sqrt(3Td_today/mη);
    s = 4mη^2 / (1-vrel^2);
    Λ, N, L1 = model.Λ, model.N, model.L1
    fη = Λ * sqrt(N) / 4π
    (8L1^2*(376mη^8 - 576mη^6*s + 396mη^4*s^2 - 136mη^2*s^3 + 21s^4))/(15fη^4 * Λ^4)
end

function Neff_cmb(model::DarkSUNModel)

end

"""
    scan_sensitivity(Λs, Ns)

Compute the relic densities for the η and Δ in the for each value in `μηs` and
for each value in `Λs` and `Ns`.
"""
function scan_sensitivity(Λs, Ns, L1s, cs, μηs, μΔs, ξinfs, has_dps)
    df = DataFrame(
        Λ=Float64[],
        N=Float64[],
        L1=Float64[],
        c=Float64[],
        μη=Float64[],
        μΔ=Float64[],
        ξinf=Float64[],
        has_dp=Bool[],
        rdη=Float64[],
        rdΔ=Float64[],
        xstar=Float64[]
    )
    len = (length(Ns) * length(Λs) * length(ξinfs) * length(has_dps) *
           length(L1s) * length(cs) * length(μηs) * length(μΔs))
    p = Progress(
        len,
        dt=5.0,
        barglyphs=BarGlyphs('|','█', ['▁' ,'▂' ,'▃' ,'▄' ,'▅' ,'▆', '▇'],' ','|',),
        barlen=50
    )
    iter = 1
    for N in Ns, Λ in Λs, ξinf in ξinfs, has_dp in has_dps
        for L1 in L1s, c in cs, μη in μηs, μΔ in μΔs
            try
                model = DarkSUNModel(Λ, N, L1, c, μη, μΔ, ξinf, has_dp);
                # Compute the values of x for which η begins to freeze
                xstar = compute_xstar!(model)

                pf = model.η.mass * M_PLANK * sqrt(π/45);
                # Compute the RD of η
                Ystar = Yeq(xstar, model)
                α = pf * quadgk(x->alpha_integrand(x,model), xstar, Inf)[1]
                Yη = Ystar / cbrt(1 + 3α * Ystar^3)
                rdη = model.η.mass * Yη * S_TODAY / RHO_CRIT
                # Compute the RD of Δ
                YΔ = pf * quadgk(x->integrand_Δ(x, model), model.x_init, xstar)[1]
                rdΔ = model.Δ.mass * YΔ * S_TODAY / RHO_CRIT
                push!(df, [Λ, N, L1, c, μη, μΔ, ξinf, has_dp, rdη, rdΔ, xstar])
            catch
                push!(df, [Λ, N, L1, c, μη, μΔ, ξinf, has_dp, missing, missing, missing])
            end
            iter+=1
            update!(p, iter)
        end
    end
    return df
end
