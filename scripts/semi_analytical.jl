using Roots
using ForwardDiff
using Calculus
using QuadGK

include("../src/thermodynamic_particles.jl")
include("../src/standard_model.jl")
include("../src/thermal_cs.jl")
include("../src/compute_xi.jl")
include("../src/constants.jl")

mutable struct DarkSUNModel
    # Parameters
    Λ::Float64
    N::Float64
    L1::Float64
    c::Float64
    μη::Float64
    μΔ::Float64
    ξinf::Float64
    η::ThermodynamicBoson
    Δ::ThermodynamicFermion
    has_dp::Bool
    # freeze out
    has_frozen::Bool
    ξfo::Float64
    Tsmfo::Float64
    # Initializations
    x_init::Float64
    x_final::Float64
    Td_init::Float64
    Tsm_init::Float64
end

function DarkSUNModel(Λ::Float64, N::Float64, L1::Float64, c::Float64, μη::Float64, μΔ::Float64, ξinf::Float64, has_dp::Bool)
    η = ThermodynamicBoson(μη * Λ / √N, 1.0)
    Δ = ThermodynamicFermion(μΔ * Λ * N, 2.0)
    # Compute initial variables
    Td0::Float64 = η.mass
    hdinf::Float64 = dark_dof_entropy_inf(N, has_dp)
    hd::Float64 = dark_dof_entropy(Td0, η, Δ, has_dp)
    ξ0::Float64 = compute_ξ_const_Td(Td0, hd, hdinf, ξinf)
    Tsm0::Float64 = Td0 / ξ0
    xinit::Float64 = η.mass / Tsm0
    winit = [log(η.neq(Td0) / sm_entropy_density(Tsm0))]
    xfinal = η.mass / T_CMB

    return DarkSUNModel(Λ, N, L1, c, μη, μΔ, ξinf, η, Δ, has_dp, false,
                        NaN, NaN, xinit, xfinal, Td0, Tsm0)
end

@inline function dark_dof_entropy_inf(N::Float64, has_dp::Bool)
    7.0 / 2.0 * N + 2N^2 - 2.0 + (has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy_inf(model::DarkSUNModel)
    dark_dof_entropy_inf(model.N, model.has_dp)
end

@inline function sum_internal_dark_dof(m::DarkSUNModel)
    m.η.g + 7.0 / 8.0 * m.Δ.g + (m.has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy(Td::Float64, η::ThermodynamicBoson, Δ::ThermodynamicFermion, has_dp::Bool)
    η.dof_entropy(Td) + Δ.dof_entropy(Td) + (has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy(Td::U, m::DarkSUNModel) where U<:Real
    m.η.dof_entropy(Td) + m.Δ.dof_entropy(Td) + (m.has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_energy(Td::U, m::DarkSUNModel) where U<:Real
    m.η.dof_energy(Td) + m.Δ.dof_energy(Td) + (m.has_dp ? 2.0 : 0.0)
end

function thermal_cs_ηη_ηηηη(Td::U, model::DarkSUNModel) where U<:Real
    fη::Float64 = sqrt(model.N) * model.Λ / (4π)
    pf::Float64 = (model.L1^2 * model.η.mass^7 / (fη * model.Λ)^4)^2
    pf * thermal_cs_ηη_ηηηη(model.η.mass / Td, model.N)
end

function thermal_cs_ηη_ΔΔ(Td::U, model::DarkSUNModel) where U<:Real
    cs = thermal_cs_ηη_ΔΔ(Td, model.N, model.Λ, model.c, model.μΔ, model.μη)
    isfinite(cs) ? cs : 0.0
end

function compute_ξ(Tsm::U, model::DarkSUNModel) where U<:Real
    darkh(Td::U) where U<:Real = dark_dof_entropy(Td, model)
    hdinf::Float64 = dark_dof_entropy_inf(model)
    sumg::Float64 = sum_internal_dark_dof(model)
    gl::Float64 = model.has_dp ? 2.0 : model.η.g
    ml::Float64 = model.η.mass
    ξinf::Float64 = model.ξinf
    if model.has_dp
        return compute_ξ_const_Tsm(Tsm, darkh, hdinf, sumg, gl, ξinf)
        #return compute_ξ_const_Tsm(Tsm, model.ξcurrent, darkh, hdinf, ξinf)
    else
        if model.has_frozen
            # If the particle freezes out when it it relativistic, ξ=ξfo
            if model.Tsmfo * model.ξfo > model.η.mass
                return model.ξfo
            # If the particle freezes out when it it non-relativistic,
            # ξ is redshifted
            else
                return model.ξfo * Tsm / model.Tsmfo
            end
        else # no dark photon and hasn't frozen out
            return compute_ξ_const_Tsm(Tsm, darkh, hdinf, sumg, gl, ml, ξinf)
            #return compute_ξ_const_Tsm(Tsm, model.ξcurrent, darkh, hdinf, ξinf)
        end
    end
end

function Yeq(x, model::DarkSUNModel)
    T = model.η.mass / x
    ξ = compute_ξ(T, model)
    model.η.neq(ξ * T) / sm_entropy_density(T)
end

function xstar_eqn(xstar, model::DarkSUNModel)
    mη = model.η.mass
    T = mη / xstar
    ξ = compute_ξ(T, model)
    Td = T * ξ
    # yeq stuff
    yeq = Yeq(xstar, model)
    dyeq = Calculus.derivative(x -> Yeq(x, model), xstar)
    # Compute gstar
    _dark_dof_energy = dark_dof_energy(Td, model)
    _sm_dof_energy = sm_dof_energy(T)
    _dof_energy_eff = _sm_dof_energy + _dark_dof_energy * ξ^4
    sqrt_gstar = sm_sqrt_gstar(T) * sqrt(_sm_dof_energy / _dof_energy_eff)
    # ⟨σv⟩
    σηη = thermal_cs_ηη_ηηηη(Td, model)

    dyeq + yeq^2 * σηη * sqrt(π/45) * sqrt_gstar * M_PLANK * mη / xstar^2
end

function find_xstar_max(model::DarkSUNModel; maxiter = 100)
    xstarmax = 10.0
    iter = 0
    while true
        val = xstar_eqn(xstarmax, model)
        if val == 0.0
            xstarmax *= 0.9
        elseif val < 0.0
            return xstarmax
        else
            xstarmax *= 1.1
        end
        iter+=1
        if iter > maxiter
            throw("cant find xstar_max")
        end
    end
end

function find_xstar_min(model::DarkSUNModel; maxiter = 100)
    xstarmax = 0.1
    iter = 0
    while true
        val = xstar_eqn(xstarmax, model)
        if val <= 0.0
            xstarmax *= 0.9
        else
            return xstarmax
        end
        iter+=1
        if iter > maxiter
            throw("cant find xstar_max")
        end
    end
end

function compute_xstar(model::DarkSUNModel)
    xmax = find_xstar_max(model)
    xmin = find_xstar_min(model)
    f(x) = xstar_eqn(x, model)
    find_zero(f, (xmin, xmax), Bisection())
end

function alpha_integrand(x, ξstar, model::DarkSUNModel)
    mη = model.η.mass
    T = mη / x
    ξ = compute_ξ(T, model)
    Td = T * ξ
    # yeq stuff
    yeq = Yeq(x, model)
    # Compute gstar
    _dark_dof_energy = dark_dof_energy(Td, model)
    _sm_dof_energy = sm_dof_energy(T)
    _dof_energy_eff = _sm_dof_energy + _dark_dof_energy * ξ^4
    sqrt_gstar = sm_sqrt_gstar(T) * sqrt(_sm_dof_energy / _dof_energy_eff)
    # ⟨σv⟩
    σηη = thermal_cs_ηη_ηηηη(Td, model)

    sqrt_gstar * σηη / (x * yeq)^2
end

function compute_alpha(xstar, model::DarkSUNModel)
    integral = quadgk(x->alpha_integrand(x,model), xstar, Inf)[1]
    model.η.mass * M_PLANK * sqrt(π/45)
end

function compute_yf(model::DarkSUNModel)
    xstar = compute_xstar(model)
    α = compute_alpha(xstar, model)
    Ys = Yeq(xstar, model)
    Ys / cbrt(1 + 3α * Ys^3)
end

function integrand_Δ(x, model::DarkSUNModel)
    mη = model.η.mass
    T = mη / x
    ξ = compute_ξ(T, model)
    Td = T * ξ
    # yeq stuff
    yeq = Yeq(x, model)
    # Compute gstar
    _dark_dof_energy = dark_dof_energy(Td, model)
    _sm_dof_energy = sm_dof_energy(T)
    _dof_energy_eff = _sm_dof_energy + _dark_dof_energy * ξ^4
    sqrt_gstar = sm_sqrt_gstar(T) * sqrt(_sm_dof_energy / _dof_energy_eff)
    σ = thermal_cs_ηη_ΔΔ(Td, model.N, model.Λ, model.c, model.μΔ, model.μη)

    σ * sqrt_gstar * yeq^2 / x^2
end

function compute_yf_Δ(model::DarkSUNModel)
    xstar = compute_xstar(model)
    xi = model.η.mass / model.Tsm_init
    f(x) = integrand_Δ(x, model)
    sqrt(π/45) * M_PLANK * model.η.mass * quadgk(f, xi, xstar)[1]
end

function compute_rd_η(model::DarkSUNModel)
    Yf = compute_yf(model)
    model.η.mass * Yf * S_TODAY / RHO_CRIT
end

function compute_rd_Δ(model::DarkSUNModel)
    Yf = compute_yf_Δ(model)
    model.Δ.mass * Yf * S_TODAY / RHO_CRIT
end
