using Rambo
using Plots;
using LaTeXStrings
using Interpolations
using LsqFit
using SpecialFunctions
using QuadGK
using DelimitedFiles

"""
Squared matrix element for 2η' → 4η' with all momenta rescaled by mη.

# Arguments
- `momenta::Array{FourMomentum, 1}`: four-momenta of final-state η's
"""
function scaled_msqrd(momenta)
  # Extract the final state eta' momenta
  q3, q4, q5, q6 = momenta
  # Extract the center of mass energy
  Q::Float64 = sum(momenta).e
  # Compute the magnitude of the initial state eta' 3-momentum
  p::Float64 = sqrt(0.25Q^2 - scalar_product(q3, q3))
  # Chose the initial state eta's to be in the CM frame traveling
  # along z-axis
  q1 = FourMomentum(0.5Q, 0.0, 0.0, p)
  q2 = FourMomentum(0.5Q, 0.0, 0.0, -p)

  return (64 *
          (((scalar_product(q2, q3) *
             (-1 + 2 * scalar_product(q2, q6) - 2 * scalar_product(q3, q6)) +
             scalar_product(q3, q6) -
             scalar_product(q2, q6) * (1 + 2 * scalar_product(q3, q6))) *
            ((scalar_product(q1, q2) - scalar_product(q1, q3) -
              scalar_product(q1, q6)) * scalar_product(q4, q5) +
             scalar_product(q1, q5) *
             (scalar_product(q2, q4) - scalar_product(q3, q4) -
              scalar_product(q4, q6)) +
             scalar_product(q1, q4) *
             (scalar_product(q2, q5) - scalar_product(q3, q5) -
              scalar_product(q5, q6)))) /
           (2 - 2 * scalar_product(q2, q3) - 2 * scalar_product(q2, q6) +
            2 * scalar_product(q3, q6)) +
           ((scalar_product(q2, q3) *
             (-1 + 2 * scalar_product(q2, q5) - 2 * scalar_product(q3, q5)) +
             scalar_product(q3, q5) -
             scalar_product(q2, q5) * (1 + 2 * scalar_product(q3, q5))) *
            (scalar_product(q1, q6) *
             (scalar_product(q2, q4) - scalar_product(q3, q4) -
              scalar_product(q4, q5)) +
             (scalar_product(q1, q2) - scalar_product(q1, q3) -
              scalar_product(q1, q5)) * scalar_product(q4, q6) +
             scalar_product(q1, q4) *
             (scalar_product(q2, q6) - scalar_product(q3, q6) -
              scalar_product(q5, q6)))) /
           (2 - 2 * scalar_product(q2, q3) - 2 * scalar_product(q2, q5) +
            2 * scalar_product(q3, q5)) +
           ((scalar_product(q2, q4) *
             (-1 + 2 * scalar_product(q2, q6) - 2 * scalar_product(q4, q6)) +
             scalar_product(q4, q6) -
             scalar_product(q2, q6) * (1 + 2 * scalar_product(q4, q6))) *
            ((scalar_product(q1, q2) - scalar_product(q1, q4) -
              scalar_product(q1, q6)) * scalar_product(q3, q5) +
             scalar_product(q1, q5) *
             (scalar_product(q2, q3) - scalar_product(q3, q4) -
              scalar_product(q3, q6)) +
             scalar_product(q1, q3) *
             (scalar_product(q2, q5) - scalar_product(q4, q5) -
              scalar_product(q5, q6)))) /
           (2 - 2 * scalar_product(q2, q4) - 2 * scalar_product(q2, q6) +
            2 * scalar_product(q4, q6)) +
           ((scalar_product(q2, q4) *
             (-1 + 2 * scalar_product(q2, q5) - 2 * scalar_product(q4, q5)) +
             scalar_product(q4, q5) -
             scalar_product(q2, q5) * (1 + 2 * scalar_product(q4, q5))) *
            (scalar_product(q1, q6) *
             (scalar_product(q2, q3) - scalar_product(q3, q4) -
              scalar_product(q3, q5)) +
             (scalar_product(q1, q2) - scalar_product(q1, q4) -
              scalar_product(q1, q5)) * scalar_product(q3, q6) +
             scalar_product(q1, q3) *
             (scalar_product(q2, q6) - scalar_product(q4, q6) -
              scalar_product(q5, q6)))) /
           (2 - 2 * scalar_product(q2, q4) - 2 * scalar_product(q2, q5) +
            2 * scalar_product(q4, q5)) +
           ((scalar_product(q2, q3) *
             (-1 + 2 * scalar_product(q2, q4) - 2 * scalar_product(q3, q4)) +
             scalar_product(q3, q4) -
             scalar_product(q2, q4) * (1 + 2 * scalar_product(q3, q4))) *
            (scalar_product(q1, q6) *
             (scalar_product(q2, q5) - scalar_product(q3, q5) -
              scalar_product(q4, q5)) +
             scalar_product(q1, q5) *
             (scalar_product(q2, q6) - scalar_product(q3, q6) -
              scalar_product(q4, q6)) +
             (scalar_product(q1, q2) - scalar_product(q1, q3) -
              scalar_product(q1, q4)) * scalar_product(q5, q6))) /
           (2 - 2 * scalar_product(q2, q3) - 2 * scalar_product(q2, q4) +
            2 * scalar_product(q3, q4)) +
           ((scalar_product(q4, q6) +
             scalar_product(q3, q6) * (1 + 2 * scalar_product(q4, q6)) +
             scalar_product(q3, q4) *
             (1 + 2 * scalar_product(q3, q6) + 2 * scalar_product(q4, q6))) *
            ((scalar_product(q1, q3) + scalar_product(q1, q4) +
              scalar_product(q1, q6)) * scalar_product(q2, q5) +
             scalar_product(q1, q5) *
             (scalar_product(q2, q3) + scalar_product(q2, q4) +
              scalar_product(q2, q6)) +
             scalar_product(q1, q2) *
             (scalar_product(q3, q5) + scalar_product(q4, q5) +
              scalar_product(q5, q6)))) /
           (2 + 2 * scalar_product(q3, q4) + 2 * scalar_product(q3, q6) +
            2 * scalar_product(q4, q6)) +
           ((scalar_product(q4, q5) +
             scalar_product(q3, q5) * (1 + 2 * scalar_product(q4, q5)) +
             scalar_product(q3, q4) *
             (1 + 2 * scalar_product(q3, q5) + 2 * scalar_product(q4, q5))) *
            (scalar_product(q1, q6) *
             (scalar_product(q2, q3) + scalar_product(q2, q4) +
              scalar_product(q2, q5)) +
             (scalar_product(q1, q3) + scalar_product(q1, q4) +
              scalar_product(q1, q5)) * scalar_product(q2, q6) +
             scalar_product(q1, q2) *
             (scalar_product(q3, q6) + scalar_product(q4, q6) +
              scalar_product(q5, q6)))) /
           (2 + 2 * scalar_product(q3, q4) + 2 * scalar_product(q3, q5) +
            2 * scalar_product(q4, q5)) +
           (((scalar_product(q1, q2) - scalar_product(q1, q5) -
              scalar_product(q1, q6)) * scalar_product(q3, q4) +
             scalar_product(q1, q4) *
             (scalar_product(q2, q3) - scalar_product(q3, q5) -
              scalar_product(q3, q6)) +
             scalar_product(q1, q3) *
             (scalar_product(q2, q4) - scalar_product(q4, q5) -
              scalar_product(q4, q6))) *
            (scalar_product(q2, q5) *
             (-1 + 2 * scalar_product(q2, q6) - 2 * scalar_product(q5, q6)) +
             scalar_product(q5, q6) -
             scalar_product(q2, q6) * (1 + 2 * scalar_product(q5, q6)))) /
           (2 - 2 * scalar_product(q2, q5) - 2 * scalar_product(q2, q6) +
            2 * scalar_product(q5, q6)) +
           (((scalar_product(q1, q3) + scalar_product(q1, q5) +
              scalar_product(q1, q6)) * scalar_product(q2, q4) +
             scalar_product(q1, q4) *
             (scalar_product(q2, q3) + scalar_product(q2, q5) +
              scalar_product(q2, q6)) +
             scalar_product(q1, q2) *
             (scalar_product(q3, q4) + scalar_product(q4, q5) +
              scalar_product(q4, q6))) *
            (scalar_product(q5, q6) +
             scalar_product(q3, q6) * (1 + 2 * scalar_product(q5, q6)) +
             scalar_product(q3, q5) *
             (1 + 2 * scalar_product(q3, q6) + 2 * scalar_product(q5, q6)))) /
           (2 + 2 * scalar_product(q3, q5) + 2 * scalar_product(q3, q6) +
            2 * scalar_product(q5, q6)) +
           (((scalar_product(q1, q4) + scalar_product(q1, q5) +
              scalar_product(q1, q6)) * scalar_product(q2, q3) +
             scalar_product(q1, q3) *
             (scalar_product(q2, q4) + scalar_product(q2, q5) +
              scalar_product(q2, q6)) +
             scalar_product(q1, q2) *
             (scalar_product(q3, q4) + scalar_product(q3, q5) +
              scalar_product(q3, q6))) *
            (scalar_product(q5, q6) +
             scalar_product(q4, q6) * (1 + 2 * scalar_product(q5, q6)) +
             scalar_product(q4, q5) *
             (1 + 2 * scalar_product(q4, q6) + 2 * scalar_product(q5, q6)))) /
           (2 + 2 * scalar_product(q4, q5) + 2 * scalar_product(q4, q6) +
            2 * scalar_product(q5, q6))))^2
end

function msqrd(momenta)
  # Extract the final state eta' momenta
  p3, p4, p5, p6 = momenta
  # Extract the center of mass energy
  Q::Float64 = sum(momenta).e
  meta = Rambo.mass(p3)
  # Compute the magnitude of the initial state eta' 3-momentum
  p::Float64 = sqrt(0.25Q^2 - meta^2)
  # Chose the initial state eta's to be in the CM frame traveling
  # along z-axis
  p1 = FourMomentum(0.5Q, 0.0, 0.0, p)
  p2 = FourMomentum(0.5Q, 0.0, 0.0, -p)

  return (64 *
          (((scalar_product(p2, p3) *
             (-meta^2 + 2 * scalar_product(p2, p6) -
              2 * scalar_product(p3, p6)) + meta^2 * scalar_product(p3, p6) -
             scalar_product(p2, p6) * (meta^2 + 2 * scalar_product(p3, p6))) *
            ((scalar_product(p1, p2) - scalar_product(p1, p3) -
              scalar_product(p1, p6)) * scalar_product(p4, p5) +
             scalar_product(p1, p5) *
             (scalar_product(p2, p4) - scalar_product(p3, p4) -
              scalar_product(p4, p6)) +
             scalar_product(p1, p4) *
             (scalar_product(p2, p5) - scalar_product(p3, p5) -
              scalar_product(p5, p6)))) /
           (2 * meta^2 - 2 * scalar_product(p2, p3) -
            2 * scalar_product(p2, p6) + 2 * scalar_product(p3, p6)) +
           ((scalar_product(p2, p3) *
             (-meta^2 + 2 * scalar_product(p2, p5) -
              2 * scalar_product(p3, p5)) + meta^2 * scalar_product(p3, p5) -
             scalar_product(p2, p5) * (meta^2 + 2 * scalar_product(p3, p5))) *
            (scalar_product(p1, p6) *
             (scalar_product(p2, p4) - scalar_product(p3, p4) -
              scalar_product(p4, p5)) +
             (scalar_product(p1, p2) - scalar_product(p1, p3) -
              scalar_product(p1, p5)) * scalar_product(p4, p6) +
             scalar_product(p1, p4) *
             (scalar_product(p2, p6) - scalar_product(p3, p6) -
              scalar_product(p5, p6)))) /
           (2 * meta^2 - 2 * scalar_product(p2, p3) -
            2 * scalar_product(p2, p5) + 2 * scalar_product(p3, p5)) +
           ((scalar_product(p2, p4) *
             (-meta^2 + 2 * scalar_product(p2, p6) -
              2 * scalar_product(p4, p6)) + meta^2 * scalar_product(p4, p6) -
             scalar_product(p2, p6) * (meta^2 + 2 * scalar_product(p4, p6))) *
            ((scalar_product(p1, p2) - scalar_product(p1, p4) -
              scalar_product(p1, p6)) * scalar_product(p3, p5) +
             scalar_product(p1, p5) *
             (scalar_product(p2, p3) - scalar_product(p3, p4) -
              scalar_product(p3, p6)) +
             scalar_product(p1, p3) *
             (scalar_product(p2, p5) - scalar_product(p4, p5) -
              scalar_product(p5, p6)))) /
           (2 * meta^2 - 2 * scalar_product(p2, p4) -
            2 * scalar_product(p2, p6) + 2 * scalar_product(p4, p6)) +
           ((scalar_product(p2, p4) *
             (-meta^2 + 2 * scalar_product(p2, p5) -
              2 * scalar_product(p4, p5)) + meta^2 * scalar_product(p4, p5) -
             scalar_product(p2, p5) * (meta^2 + 2 * scalar_product(p4, p5))) *
            (scalar_product(p1, p6) *
             (scalar_product(p2, p3) - scalar_product(p3, p4) -
              scalar_product(p3, p5)) +
             (scalar_product(p1, p2) - scalar_product(p1, p4) -
              scalar_product(p1, p5)) * scalar_product(p3, p6) +
             scalar_product(p1, p3) *
             (scalar_product(p2, p6) - scalar_product(p4, p6) -
              scalar_product(p5, p6)))) /
           (2 * meta^2 - 2 * scalar_product(p2, p4) -
            2 * scalar_product(p2, p5) + 2 * scalar_product(p4, p5)) +
           ((scalar_product(p2, p3) *
             (-meta^2 + 2 * scalar_product(p2, p4) -
              2 * scalar_product(p3, p4)) + meta^2 * scalar_product(p3, p4) -
             scalar_product(p2, p4) * (meta^2 + 2 * scalar_product(p3, p4))) *
            (scalar_product(p1, p6) *
             (scalar_product(p2, p5) - scalar_product(p3, p5) -
              scalar_product(p4, p5)) +
             scalar_product(p1, p5) *
             (scalar_product(p2, p6) - scalar_product(p3, p6) -
              scalar_product(p4, p6)) +
             (scalar_product(p1, p2) - scalar_product(p1, p3) -
              scalar_product(p1, p4)) * scalar_product(p5, p6))) /
           (2 * meta^2 - 2 * scalar_product(p2, p3) -
            2 * scalar_product(p2, p4) + 2 * scalar_product(p3, p4)) +
           ((meta^2 * scalar_product(p4, p6) +
             scalar_product(p3, p6) * (meta^2 + 2 * scalar_product(p4, p6)) +
             scalar_product(p3, p4) *
             (meta^2 + 2 * scalar_product(p3, p6) +
              2 * scalar_product(p4, p6))) *
            ((scalar_product(p1, p3) + scalar_product(p1, p4) +
              scalar_product(p1, p6)) * scalar_product(p2, p5) +
             scalar_product(p1, p5) *
             (scalar_product(p2, p3) + scalar_product(p2, p4) +
              scalar_product(p2, p6)) +
             scalar_product(p1, p2) *
             (scalar_product(p3, p5) + scalar_product(p4, p5) +
              scalar_product(p5, p6)))) /
           (2 * meta^2 + 2 * scalar_product(p3, p4) +
            2 * scalar_product(p3, p6) + 2 * scalar_product(p4, p6)) +
           ((meta^2 * scalar_product(p4, p5) +
             scalar_product(p3, p5) * (meta^2 + 2 * scalar_product(p4, p5)) +
             scalar_product(p3, p4) *
             (meta^2 + 2 * scalar_product(p3, p5) +
              2 * scalar_product(p4, p5))) *
            (scalar_product(p1, p6) *
             (scalar_product(p2, p3) + scalar_product(p2, p4) +
              scalar_product(p2, p5)) +
             (scalar_product(p1, p3) + scalar_product(p1, p4) +
              scalar_product(p1, p5)) * scalar_product(p2, p6) +
             scalar_product(p1, p2) *
             (scalar_product(p3, p6) + scalar_product(p4, p6) +
              scalar_product(p5, p6)))) /
           (2 * meta^2 + 2 * scalar_product(p3, p4) +
            2 * scalar_product(p3, p5) + 2 * scalar_product(p4, p5)) +
           (((scalar_product(p1, p2) - scalar_product(p1, p5) -
              scalar_product(p1, p6)) * scalar_product(p3, p4) +
             scalar_product(p1, p4) *
             (scalar_product(p2, p3) - scalar_product(p3, p5) -
              scalar_product(p3, p6)) +
             scalar_product(p1, p3) *
             (scalar_product(p2, p4) - scalar_product(p4, p5) -
              scalar_product(p4, p6))) *
            (scalar_product(p2, p5) *
             (-meta^2 + 2 * scalar_product(p2, p6) -
              2 * scalar_product(p5, p6)) + meta^2 * scalar_product(p5, p6) -
             scalar_product(p2, p6) * (meta^2 + 2 * scalar_product(p5, p6)))) /
           (2 * meta^2 - 2 * scalar_product(p2, p5) -
            2 * scalar_product(p2, p6) + 2 * scalar_product(p5, p6)) +
           (((scalar_product(p1, p3) + scalar_product(p1, p5) +
              scalar_product(p1, p6)) * scalar_product(p2, p4) +
             scalar_product(p1, p4) *
             (scalar_product(p2, p3) + scalar_product(p2, p5) +
              scalar_product(p2, p6)) +
             scalar_product(p1, p2) *
             (scalar_product(p3, p4) + scalar_product(p4, p5) +
              scalar_product(p4, p6))) *
            (meta^2 * scalar_product(p5, p6) +
             scalar_product(p3, p6) * (meta^2 + 2 * scalar_product(p5, p6)) +
             scalar_product(p3, p5) *
             (meta^2 + 2 * scalar_product(p3, p6) +
              2 * scalar_product(p5, p6)))) /
           (2 * meta^2 + 2 * scalar_product(p3, p5) +
            2 * scalar_product(p3, p6) + 2 * scalar_product(p5, p6)) +
           (((scalar_product(p1, p4) + scalar_product(p1, p5) +
              scalar_product(p1, p6)) * scalar_product(p2, p3) +
             scalar_product(p1, p3) *
             (scalar_product(p2, p4) + scalar_product(p2, p5) +
              scalar_product(p2, p6)) +
             scalar_product(p1, p2) *
             (scalar_product(p3, p4) + scalar_product(p3, p5) +
              scalar_product(p3, p6))) *
            (meta^2 * scalar_product(p5, p6) +
             scalar_product(p4, p6) * (meta^2 + 2 * scalar_product(p5, p6)) +
             scalar_product(p4, p5) *
             (meta^2 + 2 * scalar_product(p4, p6) +
              2 * scalar_product(p5, p6)))) /
           (2 * meta^2 + 2 * scalar_product(p4, p5) +
            2 * scalar_product(p4, p6) + 2 * scalar_product(p5, p6))))^2
end


begin
  mη = 100.0
  # Pick CM energies from 4*mη → 100*mη
  zs = 10 .^ (range(log10(4.0 + 1e-5), stop = log10(100), length = 400))
  # number of events to generate for each phase space integral
  nevents = 10000
  # Compute the values of the scaled phase-space
  σ̄s = [integrate_phase_space(
    z,
    [1.0, 1.0, 1.0, 1.0];
    nevents = 10000, msqrd = scaled_msqrd
  )[1] for z in zs]
  σ̄s = [σ̄ / (2z * sqrt(z^2 - 4)) for (σ̄, z) in zip(σ̄s, zs)]

  plot(zs, σ̄s, label = "scaled", framestyle=:box)
  yaxis!(:log10)
  xaxis!(:log10)
  xlabel!(L"$z = \sqrt{s}/m_{\eta'}$")
  ylabel!(L"$\bar{\sigma}(z)$")
  ylims!(1e-9, 1e22)
end


begin
    # Generate new, dense data set for the scaled phase-space
  zs = 10 .^ (range(log10(4.0 + 1e-5), stop = log10(500), length = 1000))
  σ̄s = [integrate_phase_space(
    z,
    [1.0, 1.0, 1.0, 1.0];
    nevents = 50000,
    msqrd = scaled_msqrd
  )[1] for z in zs]
  σ̄s = [σ̄ / (2z * sqrt(z^2 - 4)) for (σ̄, z) in zip(σ̄s, zs)]
  # Generate a linear (k = 1) spline of the data
  spl = LinearInterpolation(zs, σ̄s)
end

begin
    # Model the large energy behavior as ps(cme) = cme^p₁ * 10^p₂
  @. model(x, p) = x^p[1] * 10^p[2]
    # Take only the last twenty elements of the data to capture large energies
  xdata = zs[end-30:end]
  ydata = σ̄s[end-30:end]
  fit = curve_fit(model, xdata, ydata, [14.0, -14.0])
  m, b = coef(fit)
end

"""
Computes the cross section for 2η → 4η at zero temperature.

# Arguments
- `x::Float64`: η' mass scaled by temperature.
"""
function cs_interp(z::Float64)
  if z < zs[1]
    return 0.0
  elseif z > zs[end]
    return z^m * 10^b
  else
    return spl(z)
  end
end;

begin
  zs_ext = 10 .^ (range(log10(4.0 + 1e-5), stop = log10(550), length = 500))
  plot(zs, σ̄s, lw = 3, alpha = 0.6, label = "data")
  plot!(zs_ext, cs_interp.(zs_ext))
  yaxis!(:log10)
  xaxis!(:log10)
  xlabel!(L"$\sqrt{s}$", fontsize = 16)
  ylabel!(L"\bar{\sigma}", fontsize = 16)
end

# Save the zero-temperature scaled cross section data
begin
    # Write head which will include interpolation fit
  open(string(@__DIR__) * "/eta_scaled_cs_large_energy_params_data.csv", "w") do io
    write(io, "# slope, intercept\n")
    write(io, "$m, $b\n")
  end
  open(string(@__DIR__) * "/eta_scaled_cs_data.csv", "w") do io
    writedlm(io, zip(zs, σ̄s), ',')
  end
end


"""
Thermal kernal for computing thermally averaged cross section.

# Arguments
- `z::Float64`: center of mass energy scaled by the temperature.
- `x::Float64`: η' mass scaled by temperature.
"""
function thermal_kernal(z::Float64, x::Float64)
  return z^2 * (z^2 - 4) * besselk(1, x*z)
end

"""
Computes the thermal cross section with couplings factored out.

# Arguments
- `x::Float64`: η' mass scaled by temperature.
"""
function thermal_cs(x, N)
  function integrand(z)
    return thermal_kernal(z, x) * cs_interp(z)
  end
  return x * quadgk(integrand, 4, N)[1] / (4besselk(2, x)^2)
end

open(string(@__DIR__) * "/eta_tcs_data.csv", "w") do io
  writedlm(io, zip(xs, tcs), ',')
end

plt.close_figs()

begin
  xs = 10 .^ (range(-4, stop = log10(170), length = 500))

  plt.figure(dpi = 100)
  tcs1 = [thermal_cs(x, 10) for x in xs]
  tcs2 = [thermal_cs(x, 50) for x in xs]
  tcs3 = [thermal_cs(x, 100) for x in xs]

  plt.plot(xs, tcs1, label=L"$N=10$")
  plt.plot(xs, 13364.030328224277 * xs .^ -16)
  plt.plot(xs, tcs2, label=L"$N=50$")
  plt.plot(xs, tcs3, label=L"$N=100$")

  plt.yscale("log")
  plt.xscale("log")
  plt.ylabel(
    L"$\langle\sigma v\rangle\times\left(\frac{f_{\eta}^4\Lambda^4}{m_{\eta}^7\tilde{L}_{1}^2}\right)^2$",
    fontsize = 16
  )
  plt.xlabel(L"$x$", fontsize = 16)
  plt.legend()
  plt.ylim([1e-10,1e21])
  plt.xlim([1e-4,20])
  plt.gcf()
end


begin
    # Model the large energy behavior as ps(cme) = cme^p₁ * 10^p₂
  @. model(x, p) = p[1] / x^16
    # Take only the last twenty elements of the data to capture large energies
  xdata = xs[end-100:end]
  ydata = tcs3[end-100:end]
  fit = curve_fit(model, xdata, ydata, [10.])
  coef(fit)
end

plt.plot()

@show coef(fit)[1]
