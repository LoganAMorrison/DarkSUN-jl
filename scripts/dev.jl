using Revise
using DarkSUN
using DifferentialEquations
using ODEInterfaceDiffEq
using Plots

function solve_boltzman(Λ::Float64, N::Float64, L1::Float64, c::Float64,
                        μη::Float64, μΔ::Float64, ξinf::Float64, has_dp::Bool)
    model = DarkSUNModel(Λ, N, L1, c, μη, μΔ, ξinf, has_dp)
    logxspan = (log(model.x_init), log(model.x_final))
    ff = ODEFunction(boltzmann2!; jac=jacobian2!)
    prob = ODEProblem(ff, model.w_init, logxspan, model)

    function stop_integrator(u, t, integrator)
        Tsm = exp(-integrator.t) * model.η.mass
        ξ = compute_ξ(Tsm, model)
        weq = log(model.η.neq(ξ * Tsm) / sm_entropy_density(Tsm))
        deriv = maximum(abs.(boltzmann!(u, model, t)))
        deriv < 1e-4 && model.has_frozen
    end

    cb = DiscreteCallback(stop_integrator, terminate!)
    sol = solve(prob, Rosenbrock23(), abstol=1e-5, callback=cb)
end

function solve_boltzman_radau(Λ::Float64, N::Float64, L1::Float64, c::Float64,
                              μη::Float64, μΔ::Float64, ξinf::Float64, has_dp::Bool)
    model = DarkSUNModel(Λ, N, L1, c, μη, μΔ, ξinf, has_dp)
    logxspan = (log(model.x_init), log(model.x_final))
    ff = ODEFunction(boltzmann2!; jac=jacobian2!)
    prob = ODEProblem(ff, model.w_init, logxspan, model)
    sol = solve(prob, radau(), abstol=1e-10)
end

sol = solve_boltzman_radau(1e2, 10.0, 1.0, 1.0, 1.0, 1.0, 1e-3, false)
plot(sol)
log(sol.prob.p.η.mass / sol.prob.p.Tsmfo)
exp(-sol[1,end]) * sol.prob.p.η.mass * 2891.2 / 1.05375e-5

sol.prob.p.has_frozen






model = DarkSUNModel(1e-1, 100.0, 1.0, 1.0, 1.0, 1.0, 1e-2, false)

logxs = range(log(model.x_init), stop=log(model.x_final), length=100)
Ts = model.η.mass .* exp.(-logxs)
ξs = [compute_ξ(T, model) for T in Ts]
plot!(logxs, ξs, yscale=:log10)

model.Td_init / model.Tsm_init

ξs[2]
