using Rambo
using QuadGK
using SpecialFunctions
using Interpolations
using Plots
import PyPlot; const plt = PyPlot;

function nbar(x::T) where T<:Real
    if x < 100.0
        return x^2 * sum([1 / n * besselk(2, n*x) for n in 1:5]) / (2π^2)
    else
        return x^2 * exp(-x)* sqrt(π/2) / sqrt(x) / (2π^2)
    end
end

function scaled_msqrd(momenta)
  # Extract the center of mass energy
  Q::Float64 = sum(momenta).e
  # Compute the magnitude of the initial state eta' 3-momentum
  p::Float64 = sqrt(Q^2 / 4 - 1)
  # Chose the initial state eta's to be in the CM frame traveling
  # along z-axis
  q1 = FourMomentum(Q / 2, 0.0, 0.0, p)
  q2 = FourMomentum(Q / 2, 0.0, 0.0, -p)
  # Extract the final state eta' momenta
  q3, q4, q5, q6 = momenta

  return (528 *(
    scalar_product(q1,q4)*scalar_product(q2,q6)*scalar_product(q3,q5) +
    scalar_product(q1,q4)*scalar_product(q2,q5)*scalar_product(q3,q6) +
    scalar_product(q1,q3)*scalar_product(q2,q6)*scalar_product(q4,q5) +
    scalar_product(q1,q2)*scalar_product(q3,q6)*scalar_product(q4,q5) +
    scalar_product(q1,q6)*(scalar_product(q2,q5)*scalar_product(q3,q4) +
    scalar_product(q2,q4)*scalar_product(q3,q5) +
    scalar_product(q2,q3)*scalar_product(q4,q5)) +
    scalar_product(q1,q3)*scalar_product(q2,q5)*scalar_product(q4,q6) +
    scalar_product(q1,q2)*scalar_product(q3,q5)*scalar_product(q4,q6) +
    scalar_product(q1,q5)*(scalar_product(q2,q6)*scalar_product(q3,q4) +
    scalar_product(q2,q4)*scalar_product(q3,q6) +
    scalar_product(q2,q3)*scalar_product(q4,q6)) +
    scalar_product(q1,q4)*scalar_product(q2,q3)*scalar_product(q5,q6) +
    scalar_product(q1,q3)*scalar_product(q2,q4)*scalar_product(q5,q6) +
    scalar_product(q1,q2)*scalar_product(q3,q4)*scalar_product(q5,q6)))^2
end;

function scaled_msqrd2(inmomenta, outmomenta)
  q1, q2 = inmomenta
  q3, q4, q5, q6 = outmomenta

  return (528 *
          (scalar_product(q1, q4) * scalar_product(q2, q6) *
           scalar_product(q3, q5) +
           scalar_product(q1, q4) * scalar_product(q2, q5) *
           scalar_product(q3, q6) +
           scalar_product(q1, q3) * scalar_product(q2, q6) *
           scalar_product(q4, q5) +
           scalar_product(q1, q2) * scalar_product(q3, q6) *
           scalar_product(q4, q5) +
           scalar_product(q1, q6) *
           (scalar_product(q2, q5) * scalar_product(q3, q4) +
            scalar_product(q2, q4) * scalar_product(q3, q5) +
            scalar_product(q2, q3) * scalar_product(q4, q5)) +
           scalar_product(q1, q3) * scalar_product(q2, q5) *
           scalar_product(q4, q6) +
           scalar_product(q1, q2) * scalar_product(q3, q5) *
           scalar_product(q4, q6) +
           scalar_product(q1, q5) *
           (scalar_product(q2, q6) * scalar_product(q3, q4) +
            scalar_product(q2, q4) * scalar_product(q3, q6) +
            scalar_product(q2, q3) * scalar_product(q4, q6)) +
           scalar_product(q1, q4) * scalar_product(q2, q3) *
           scalar_product(q5, q6) +
           scalar_product(q1, q3) * scalar_product(q2, q4) *
           scalar_product(q5, q6) +
           scalar_product(q1, q2) * scalar_product(q3, q4) *
           scalar_product(q5, q6)))^2
end

function integrand24(Q0::Float64, x::Float64)
    msqrd = integrate_phase_space(Q0, [1.0,1.0,1.0,1.0]; nevents=500, msqrd=scaled_msqrd)
    return msqrd[1] * Q0^3 * sqrt(1-4/Q0^2) / (exp(Q0/2*x) - 1)^2 / 24
end

function integrand42(Q0::Float64, x::Float64)
    function _msqrd(momenta)
        e1::Float64 = momenta[1].e
        e2::Float64 = momenta[2].e
        e3::Float64 = momenta[3].e
        e4::Float64 = momenta[4].e
        (scaled_msqrd(momenta) / (exp(e1 * x) - 1)  /
         (exp(e2 * x) - 1)  / (exp(e3 * x) - 1) / (exp(e4 * x) - 1))
    end
    msqrd = integrate_phase_space(Q0, [1.0,1.0,1.0,1.0]; nevents=500, msqrd=_msqrd)
    return msqrd[1] * Q0^3 * sqrt(1-4/Q0^2)
end

function sigma24(x::Float64, N::Float64)
    Qmax::Float64 = 10.0
    Qmin::Float64 = 4.0 + 1e-3
    # Find good value for qmax
    err::Float64 = integrand24(Qmax, x)
    while err > 1e-10
        Qmax *= 2
        err = integrand24(Qmax, x)
    end
    Qmax = min(Qmax,N)
    Q0s = [Q0 for Q0 in range(Qmin, stop=Qmax, length=100)]
    pss = [integrand24(Q0, x) for Q0 in Q0s]
    integrand = LinearInterpolation(Q0s, pss, extrapolation_bc=Flat())
    #integrand(Q0::Float64) = integrand24(Q0, x)
    (x^2 / 4π)^3 / nbar(x)^2 * quadgk(integrand, Qmin, Qmax)[1]
end

function sigma42(x::Float64, N::Float64)
    Qmax::Float64 = 10.0
    Qmin::Float64 = 4.0 + 1e-3
    # Find good value for qmax
    err::Float64 = integrand42(Qmax, x)
    while err > 1e-10
        Qmax *= 2
        err = integrand42(Qmax, x)
    end
    Qmax = min(Qmax,N)
    Q0s = [Q0 for Q0 in range(Qmin, stop=Qmax, length=500)]
    pss = [integrand42(Q0, x) for Q0 in Q0s]
    integrand = LinearInterpolation(Q0s, pss, extrapolation_bc=Flat())
    (x^4 / 4π)^3 / nbar(x)^4 * quadgk(integrand, Qmin, Qmax)[1]
end

xs = range(0.01, stop=10.0, length=100)
Ns = [10.0, 50.0, 100.0]
begin
    plot( yscale=:log10, xscale=:log10)
    for N in Ns
        σ24s = [sigma24(x, N) for x in xs]
        plot!(xs, σ24s, label="N = $N")
    end
    plot!()
end

begin
    plot(yscale=:log10, xscale=:log10)
    for N in Ns
        σ42s = [sigma42(x, N) for x in xs]
        plot!(xs, σ42s, label="N = $N")
    end
    plot!()
end

begin
    plot(yscale=:log10, xscale=:log10)
    for N in Ns
        σ24s = [sigma24(x, N) for x in xs]
        σ42s = [nbar(x)^2 / x^6 * sigma42(x, N) for x in xs]
        plot!(xs, σ24s, label="N = $N, 24")
        plot!(xs, σ42s, linestyle=:dash, label="N = $N, 42")
    end
    plot!()
end

σ24s

σ24s = [sigma24(x, 10.0) for x in xs]
σ42s = [nbar(x)^2 / x^6 * sigma42(x, 10.0) for x in xs]



function integrand24(Q::Float64, x::Float64)
    function _msqrd(in, out)
        ϵ1::Float64 = in[1].e
        ϵ2::Float64 = in[2].e
        scaled_msqrd2(in, out) / (exp(ϵ1 * x)) / (exp(ϵ2 * x))
    end

    val = 0.0
    npts = 1000
    for i in 1:npts
      pt1 = generate_phase_space_point(10.0, [1.0, 1.0])
      pt2 = generate_phase_space_point(10.0, [1.0, 1.0, 1.0, 1.0])
      inmomenta = pt1.four_momenta
      outmomenta = pt2.four_momenta
      val += pt1.weight * pt2.weight * _msqrd(inmomenta, outmomenta)
    end
    val / npts * Q^3
end

function integrand42(Q::Float64, x::Float64)
    function _msqrd(in, out)
        ϵ1::Float64 = out[1].e
        ϵ2::Float64 = out[2].e
        ϵ3::Float64 = out[3].e
        ϵ4::Float64 = out[4].e
        scaled_msqrd2(in, out) / (exp(ϵ1 * x)) / (exp(ϵ2 * x)) /
        (exp(ϵ3 * x)) / (exp(ϵ4 * x))
    end

    val = 0.0
    npts = 1000
    for i in 1:npts
        pt1 = generate_phase_space_point(10.0, [1.0, 1.0])
        pt2 = generate_phase_space_point(10.0, [1.0, 1.0, 1.0, 1.0])
        inmomenta = pt1.four_momenta
        outmomenta = pt2.four_momenta
        val += pt1.weight * pt2.weight * _msqrd(inmomenta, outmomenta)
    end
    val / npts * Q^3
end

function σ24(x::Float64, Qcut::Float64)
  Qmax::Float64 = 10.0
  Qmin::Float64 = 4.0 + 1e-5
  err::Float64 = integrand24(Qmax, x)
  while err > 1e-10 && Qmax < Qcut
    Qmax *= 2
    err = integrand24(Qmax, x)
  end
  Qmax = min(Qmax, Qcut)
  Q0s = 10 .^ range(log10(Qmin), stop=log10(Qmax), length=100)
  pss = [integrand24(Q0, x) for Q0 in Q0s]
  integrand = LinearInterpolation(Q0s, pss, extrapolation_bc=Flat())
  x^6 / nbar(x)^2 / 8π^2 * quadgk(integrand, Qmin, Qmax)[1]
end

function σ42(x::Float64, Qcut::Float64)
  Qmax::Float64 = 10.0
  Qmin::Float64 = 4.0 + 1e-5
  err::Float64 = integrand42(Qmax, x)
  while err > 1e-10 && Qmax < Qcut
    Qmax *= 2
    err = integrand42(Qmax, x)
  end
  Qmax = min(Qmax, Qcut)
  Q0s = 10 .^ range(log10(Qmin), stop=log10(Qmax), length=100)
  pss = [integrand42(Q0, x) for Q0 in Q0s]
  integrand = LinearInterpolation(Q0s, pss, extrapolation_bc=Flat())
  x^12 / 8π^2 / nbar(x)^4 * quadgk(integrand, Qmin, Qmax)[1]
end

xs = 10 .^ range(-2, stop=2, length=100);
σ24s = [σ24(x, 100.0) for x in xs];
σ42s = [σ42(x, 100.0) * nbar(x)^2 / x^6 for x in xs];

plt.close_figs()
begin
    plt.figure(dpi=100)
    plt.plot(xs, σ24s, "--", label="24", lw=3)
    plt.plot(xs, σ42s, label="42")
    plt.yscale("log")
    plt.xscale("log")
    plt.ylim([1e-3,1e20])
    plt.legend()
    plt.gcf()
end
