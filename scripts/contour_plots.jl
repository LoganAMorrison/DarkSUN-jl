using DataFrames;
using CSV;
using Plots;

df = CSV.read("/Users/loganmorrison/.julia/dev/DarkSUN/data/l1.csv")

df_no_dp = df[df.has_dp .== false,:]
df_no_dp_1 = df_no_dp[df_no_dp.L1 .== 1e-3,:];
df_no_dp_2 = df_no_dp[df_no_dp.L1 .== 2.0,:];

df_w_dp = df[df.has_dp .== true,:];
df_w_dp_1 = df_w_dp[df_w_dp.L1 .== 1e-3,:];
df_w_dp_2 = df_w_dp[df_w_dp.L1 .== 2.0,:];

Λs = unique(df_no_dp.Λ);
Ns = unique(df_no_dp.N);
rd_no_dp_mu_1 = reshape(df_no_dp_1.rdη + df_no_dp_1.rdΔ, (length(Λs),length(Ns)));
rd_no_dp_mu_2 = reshape(df_no_dp_2.rdη + df_no_dp_2.rdΔ, (length(Λs),length(Ns)));

rd_w_dp_mu_1 = reshape(df_w_dp_1.rdΔ, (length(Λs),length(Ns)));
rd_w_dp_mu_2 = reshape(df_w_dp_2.rdΔ, (length(Λs),length(Ns)));

contour(Ns, Λs, rd_no_dp_mu_1, levels=[0.1198], xaxis=:log10, yaxis=:log10)
contour!(Ns, Λs, rd_no_dp_mu_2, levels=[0.1198], xaxis=:log10, yaxis=:log10)

contour(Ns, Λs, rd_w_dp_mu_1, levels=[0.1198], xaxis=:log10, yaxis=:log10)
contour!(Ns, Λs, rd_w_dp_mu_2, levels=[0.1198], xaxis=:log10, yaxis=:log10)

rd_no_dp_mu_1
