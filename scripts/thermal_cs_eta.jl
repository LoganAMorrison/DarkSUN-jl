using DarkSUN
using DelimitedFiles
using Interpolations
using QuadGK
using Plots

function generate_cs_interp()
    # Load in interpolation data for 2η → 4η phase space
    # First load in large energy fit parameters:
    _fit_params = readdlm(
        string(@__DIR__) * "/../src/data/eta_scaled_cs_large_energy_params_data.csv",
        ',';
        skipstart = 1,
    );
    _fit_params_slope = _fit_params[1]
    _fit_params_intercpt = _fit_params[2]
    # Read in rest of data
    _data = readdlm(string(@__DIR__) * "/../src/data/eta_scaled_cs_data.csv", ',')
    _zs = _data[:, 1]
    _cs = _data[:, 2]
    _cs_interp = LinearInterpolation(_zs, _cs, extrapolation_bc=Flat())

    function cs_interp(z::Float64)
        if z < _zs[1]
            return 0.0
        elseif z > _zs[end]
            return z^_fit_params_slope * 10^_fit_params_intercpt
        else
            return _cs_interp(z)
        end
    end;

    return cs_interp
end

cs_interp = generate_cs_interp()

function tcs_ηη_ηηηη(Td::Real, model)
    x = model.Λ / sqrt(model.N) / Td
    integrand(z::Float64) = z^2 * (z^2 - 4) * besselk1(x * z) * cs_interp(z)

    quadgk(integrand, 4, model.N)[1] * x / (4 * besselk2(x)^2)
end

model = DarkSUNModel(1.0, 50, 1.0, 1.0, 1.0, 1.0, 1.0, false)

xs = 10 .^ LinRange(-3, log10(150), 100)

log10tcs = [log10(tcs_ηη_ηηηη(model.η.mass / x, model)) for x in xs]

tc_interp = LinearInterpolation(log10.(xs), log10tcs, extrapolation_bc=Flat())

plot(xs, 10 .^ tc_interp.(log10.(xs)), xaxis=:log10, yaxis=:log10)
plot!(xs, 10 .^ log10tcs, xaxis=:log10, yaxis=:log10)
ylims!(1e-10, 1e20)
