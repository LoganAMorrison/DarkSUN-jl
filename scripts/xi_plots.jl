using DarkSUN
import PyPlot; const plt=PyPlot;
using LaTeXStrings;

model1 = DarkSUNModel(1e-3, 50, 1.0, 1.0, 1.0, 1.0, 1.0, true)
model2 = DarkSUNModel(1e-3, 50, 1.0, 1.0, 1.0, 1.0, 1.0, false)

Tsms = 10 .^ LinRange(-5, 2, 100);

ξub1s = [ξ_upper_bound_const_Tsm(Tsm, model1) for Tsm in Tsms]
ξlb1s = [ξ_lower_bound_const_Tsm(Tsm, model1) for Tsm in Tsms]
ξ1s = [compute_ξ(Tsm, model1) for Tsm in Tsms]


ξub2s = [ξ_upper_bound_const_Tsm(Tsm, model2) for Tsm in Tsms]
ξlb2s = [ξ_lower_bound_const_Tsm(Tsm, model2) for Tsm in Tsms]
ξ2s = [compute_ξ(Tsm, model2) for Tsm in Tsms]

plt.figure(dpi=100)
plt.title("With Dark Photon")
plt.plot(Tsms, ξ1s)
plt.plot(Tsms, ξub1s, "--", label="Upper-bound")
plt.plot(Tsms, ξlb1s, "--", label="Lower-bound")
plt.xscale("log")
plt.xlim([Tsms[1], Tsms[end]])
plt.ylabel(L"$\xi(T_{\mathrm{SM}})$", fontsize=16)
plt.xlabel(L"$T_{\mathrm{SM}} \ (\mathrm{GeV})$", fontsize=16)
plt.legend()
plt.gcf()


plt.figure(dpi=100)
plt.title("Without Dark Photon")
plt.plot(Tsms, ξ2s)
plt.plot(Tsms, ξub2s, "--", label="Upper-bound")
plt.plot(Tsms, ξlb2s, "--", label="Lower-bound")
plt.xscale("log")
plt.xlim([Tsms[1], Tsms[end]])
plt.ylim([minimum(ξ2s) * 0.9, maximum(ξ2s) * 1.1])
plt.ylabel(L"$\xi(T_{\mathrm{SM}})$", fontsize=16)
plt.xlabel(L"$T_{\mathrm{SM}} \ (\mathrm{GeV})$", fontsize=16)
plt.legend()
plt.gcf()
