macro darksun_parameters()
    return esc(:(
        Λ::Float64;
        N::Int;
        L1::Float64;
        c::Float64;
        μη::Float64;
        μΔ::Float64;
        ξinf::Float64;
    ))
end

"Structure for the Dark SU(N) model"
struct DarkSUNModel <: ThermallyDecoupledModel
    "Confinement Scale"
    Λ::Float64
    "Degree of gauge group"
    N::Int
    "Low-energy (∂η)^4 coupling"
    L1::Float64
    "Low-energy coupling for Δ̄Δηη"
    c::Float64
    "Ratio of η mass to Λ/√N"
    μη::Float64
    "Ratio of Δ mass to ΛN"
    μΔ::Float64
    "Ratio of dark to SM temperature before confinement"
    ξinf::Float64
    "η-meson: q̄q bound state"
    η::ThermodynamicParticle
    "Δ-baryon: Nq bound state"
    Δ::ThermodynamicParticle
    "dark photon"
    γ::ThermodynamicParticle
    "thermal cs ηη→ηηηη"
    tcs_ηη_ηηηη::ThermalCrossSectionEta
    "thermal cs ηη→ΔΔ"
    tcs_ηη_ΔΔ::ThermalCrossSectionDelta
end

function DarkSUNModel(
    Λ::Float64,
    N::Int,
    L1::Float64,
    c::Float64,
    μη::Float64,
    μΔ::Float64,
    ξinf::Float64,
    has_dp::Bool,
)
    η = Boson(μη * Λ / √N, 1.0)
    # If N is even, Δ is a boson, otherwise fermion
    Δ = N % 2 == 0 ? Boson(μΔ * Λ * N, N + 1) : Fermion(μΔ * Λ * N, N + 1)
    # if we don't have a dark photon, set its d.o.f. to zero
    γ = Boson(0.0, (has_dp ? 2.0 : 0.0))

    tcs_ηη_ηηηη = ThermalCrossSectionEta(Λ, N, L1, μη)
    tcs_ηη_ΔΔ = ThermalCrossSectionDelta(Λ, N, c, μη, μΔ)

    model = DarkSUNModel(Λ, N, L1, c, μη, μΔ, ξinf, η, Δ, γ, tcs_ηη_ηηηη)
end

function setproperty!(model::DarkSUNModel, :Λ, Λ::Float64)
    model.Λ
end

"""
    dof_energy(Td::Real, model::DarkSUNModel)

Compute the d.o.f. stored in energy of the decoupled sector at a decoupled
temperature `Td`.
"""
function dof_energy(Td::Real, m::DarkSUNModel)
    dof_energy(Td, m.η) + dof_energy(Td, m.Δ) + dof_energy(Td, m.γ)
end

function dof_entropy_inf(m::DarkSUNModel)
    7.0 / 2.0 * m.N + 2 * m.N^2 - 2.0 + m.γ.g
end

function sum_internal_dof(m::DarkSUNModel)
    m.η.g + m.γ.g + (m.N % 2 == 0 ? 1.0 : 7 / 8) * m.Δ.g
end

function dof_entropy(Td::Real, m::DarkSUNModel)
    dof_entropy(Td, m.η) + dof_entropy(Td, m.Δ) + dof_entropy(Td, m.γ)
end

function ξinf(model::DarkSUNModel)
    model.ξinf
end

function dof_lightest(model::DarkSUNModel)
    model.γ.g > 0 && return model.γ.g
    model.Δ.g
end

function mass_lightest(model::DarkSUNModel)
    model.γ.g == 0 ? model.Δ.mass : 0.0
end

function ξ_upper_bound_const_Tsm(Tsm::Real, model::DarkSUNModel)
    μΔ = model.μΔ
    μη = model.μη
    _ξinf = model.ξinf
    hsm = sm_dof_entropy(Tsm)
    hdinf = dof_entropy_inf(model)

    model.γ.g > 0 && return cbrt(hsm * hdinf / model.γ.g / hsminf) * _ξinf

    xη = model.η.mass / Tsm
    xΔ = model.Δ.mass / Tsm
    pl_arg_numη = (45 * model.η.g * hsminf * xη^3)^2
    pl_arg_numΔ = (45 * model.Δ.g * hsminf * xΔ^3)^2
    pl_arg_den = (4 * hdinf * hsm * _ξinf)^2 * π^7

    ret_valη = 2xη / product_log(pl_arg_numη / pl_arg_den)
    ret_valΔ = 2xΔ / product_log(pl_arg_numΔ / pl_arg_den)
    # Return the more stringent bound
    min(ret_valη, ret_valΔ)
end

"""
    compute_ξ(Tsm::U, model::DarkSUNModel)

Compute the ratio of dark to SM temperature given a value of the SM
temperature `Tsm`
"""
function compute_ξ(Tsm::Real, model::DarkSUNModel)
    compute_ξ_const_Tsm(Tsm, model)
end

"""
    compute_ξ(Tsm::Real, ξfo, Tsmfo, model::DarkSUNModel)

Compute the ratio of dark to SM temperature after the η has frozen out at a
value of `ξfo`, a SM temperature `Tsmfo` given the current value of the SM
temperature `Tsm`.
"""
function compute_ξ(
    Tsm::Real,
    ξfo::Float64,
    Tsmfo::Float64,
    model::DarkSUNModel,
)
    # if we have a dark photon, compute ξ as usual
    model.γ.g != 0 && return compute_ξ(Tsm, model)
    Tsmfo * ξfo > model.η.mass && return ξfo # relativistic η
    ξfo * Tsm / Tsmfo # non-relativistic η
end
