abstract type ThermodynamicParticle end

mutable struct Boson <: ThermodynamicParticle
    mass::Float64
    g::Float64
end

mutable struct Fermion <: ThermodynamicParticle
    mass::Float64
    g::Float64
end

"""
    nbar(x, p::Boson)

Number density scaled by the particle `p`'s temperature and internal d.o.f evaluated at `x` = mass / T.
"""
function nbar(x::U, p::Boson) where {U<:Real}
    x == 0 && return 0.121793828233573
    return x^2 * sum([1 / n * besselk2(n * x) for n = 1:5]) / (2 * π^2)
end

"""
    nbar(x, p::Fermion)

Number density scaled by the particle `p`'s temperature and internal d.o.f evaluated at `x` = mass / T.
"""
function nbar(x::U, p::Fermion) where {U<:Real}
    x == 0 && return 0.0913453711751798
    return x^2 * sum([(-1)^(n + 1) / n * besselk2(n * x) for n = 1:5]) /
           (2 * π^2)
end

"""
    ρbar(x, p::Boson)

Energy density scaled by the particle `p`'s temperature and internal d.o.f evaluated at `x` = mass / T.
"""
function ρbar(x::U, stats::Boson) where {U<:Real}
    x == 0 && return π^2 / 30
    return x^2 / (2 * π^2) *
           sum([1 / n^2 * (x * n * besselk1(n * x) + 3 * besselk2(n * x)) for n = 1:5])
end

"""
    ρbar(x, p::Fermion)

Energy density scaled by the particle `p`'s temperature and internal d.o.f evaluated at `x` = mass / T.
"""
function ρbar(x::U, p::Fermion) where {U<:Real}
    x == 0 && return 7 * π^2 / 240
    return x^2 / (2 * π^2) * sum([(-1)^(n + 1) / n^2 *
                (x * n * besselk1(n * x) + 3 * besselk2(n * x)) for n = 1:5])
end


"""
    pbar(x, p::Boson)

Pressure density scaled by the particle `p`'s temperature and internal d.o.f evaluated at `x` = mass / T.
"""
function pbar(x::U, p::Boson) where {U<:Real}
    x == 0 && return π^2 / 90
    return x^2 / (2 * π^2) * sum([1 / n^2 * besselk2(n * x) for n = 1:5])
end

"""
    pbar(x, p::Fermion)

Pressure density scaled by the particle `p`'s temperature and internal d.o.f evaluated at `x` = mass / T.
"""
function pbar(x::U, p::Fermion) where {U<:Real}
    x == 0 && return 7 * π^2 / 720
    return x^2 / (2 * π^2) *
           sum([(-1)^(n + 1) / n^2 * besselk2(n * x) for n = 1:5])
end



"""
    sbar(x, p::Boson)

Entropy density scaled by the particle `p`'s temperature and internal d.o.f evaluated at `x` = mass / T.
"""
function sbar(x::U, p::Boson) where {U<:Real}
    x == 0 && return 2 * π^2 / 45
    return x^3 / (2 * π^2) * sum([1 / n * besselk3(n * x) for n = 1:5])
end

"""
    sbar(x, p::Fermion)

Entropy density scaled by the particle `p`'s temperature and internal d.o.f evaluated at `x` = mass / T.
"""
function sbar(x::U, p::Fermion) where {U<:Real}
    x == 0 && return 7 * π^2 / 180
    return x^3 / (2 * π^2) *
           sum([(-1)^(n + 1) / n * besselk3(n * x) for n = 1:5])
end



"""
    dof_energy(T::Real, p::ThermodynamicParticle)

Compute the d.o.f. stored in energy of particle `p` with temperature `T`.
"""
function dof_energy(T::U, p::ThermodynamicParticle) where {U<:Real}
    30 * p.g / π^2 * ρbar(p.mass / T, p)
end

"""
    dof_entropy(T::Real, p::ThermodynamicParticle)

Compute the d.o.f. stored in entropy of particle `p` with temperature `T`.
"""
function dof_entropy(T::U, p::ThermodynamicParticle) where {U<:Real}
    45 * p.g / (2 * π^2) * sbar(p.mass / T, p)
end

"""
    neq(T::Real, p::ThermodynamicParticle)

Compute the equilibrium number density of particle `p` with temperature `T`.
"""
function neq(T::U, p::ThermodynamicParticle) where {U<:Real}
    p.g * nbar(p.mass / T, p) * T^3
end

"""
    energy_density(T::Real, p::ThermodynamicParticle)

Compute the equilibrium energy density of particle `p` with temperature `T`.
"""
function energy_density(T::U, p::ThermodynamicParticle) where {U<:Real}
    p.g * ρbar(p.mass / T, p) * T^4
end

"""
    pressure_density(T::Real, p::ThermodynamicParticle)

Compute the equilibrium pressure density of particle `p` with temperature `T`.
"""
function pressure_density(T::U, p::ThermodynamicParticle) where {U<:Real}
    p.g * pbar(p.mass / T, p) * T^4
end

"""
    entropy_density(T::Real, p::ThermodynamicParticle)

Compute the equilibrium entropy density of particle `p` with temperature `T`.
"""
function entropy_density(T::U, p::ThermodynamicParticle) where {U<:Real}
    p.g * sbar(p.mass / T, p) * T^3
end
