"""
# thermally_decoupled_model.jl

Module for computing the temperature of a thermally-decoupled sector relative
to the standard model temperature.

The temperature of the thermally-decoupled sector is computed by assuming that
the entropy of the decoupled sector and SM are independently conserved. If this
is the case, then the ratio of the entropy densities of the two sectors is
a constant:
    const. = s_d / s_sm = hd(Td) / hsm(Tsm) * (Td / Tsm)^3
where s, h and T represent the entropy density, d.o.f. stored in entropy and
temperature of one of the sectors and the subsript `d` and `sm` stand for
decoupled and standard model. Given that this expression is constant, if we
evaluate it for two different times (of temperatures), then it remains the same.
In particluar, if we know the value at a given time, then we can compute the
value at later times. Suppose we know the values of ξ = Td / Tsm at a set of
temperature Tdinf, Tsminf (say after reheating), then we can compute ξ at
other temperatures using:
    ξ^3 = (hdinf / hd(Td)) (hsm(Tsm) / hsminf) * ξinf^3
Supposing we know Tsm and want to find Td, we can find Td by solving:
    ξ^3 = (hdinf / hd(ξ * Tsm)) (hsm(Tsm) / hsminf) * ξinf^3
for ξ. Likewise, we if we know Td, we can find Tsm by solving:
    ξ^3 = (hdinf / hd(Td)) (hsm(Td / ξ) / hsminf) * ξinf^3.

This module defines an abstract type `ThermallyDecoupledModel`. Models which
subtype `ThermallyDecoupledModel` must define the following functions:
    1. dof_entropy_inf(model::ThermallyDecoupledModel)
    2. sum_internal_dof(model::ThermallyDecoupledModel)
    3. dof_entropy(Td, model::ThermallyDecoupledModel)
    4. ξinf(model::ThermallyDecoupledModel)
    5. dof_lightest(model::ThermallyDecoupledModel)
    6. mass_lightest(model::ThermallyDecoupledModel)
"""

const hsminf = 106.83
const hsm0 = 3.93872

abstract type ThermallyDecoupledModel end

"""
    dof_entropy_inf(model::ThermallyDecoupledModel)

Compute the d.o.f. stored in entropy of the `ThermallyDecoupledModel` at T=∞.
"""
function dof_entropy_inf(model::ThermallyDecoupledModel)
    throw("`dof_entropy_inf` must be explicitly implemented for new models.")
end

"""
    sum_internal_dof(model::ThermallyDecoupledModel)

Sum of the internal d.o.f. of the `ThermallyDecoupledModel`.
"""
function sum_internal_dof(model::ThermallyDecoupledModel)
    throw("`sum_internal_dof` must be explicitly implemented for new models.")
end

"""
    dof_entropy(Td::Real, model)

Compute the d.o.f. stored in entropy of the decoupled sector at decoupled
temperature `Td`.
"""
function dof_entropy(Td::Real, model::ThermallyDecoupledModel)
    throw("`dof_entropy` must be explicitly implemented for new models.")
end


"""
    ξinf(model::ThermallyDecoupledModel)

Ratio of decoupled to SM temperature at T=∞ for the `ThermallyDecoupledModel`.
"""
function ξinf(m::ThermallyDecoupledModel)
    throw("`ξinf` must be explicitly implemented for new models.")
end

"""
    dof_lightest(Tsm::Real, model::ThermallyDecoupledModel)

Returns the d.o.f. of the lightest particle or the sum of the massless d.o.f.
"""
function dof_lightest(Tsm::Real, m::ThermallyDecoupledModel)
    throw("`dof_lightest` must be explicitly implemented for new models.")
end

"""
    mass_lightest(model::ThermallyDecoupledModel)

Returns the mass of the lightest particle
"""
function mass_lightest(m::ThermallyDecoupledModel)
    throw("`mass_lightest` must be explicitly implemented for new models.")
end

"""
    ξ_upper_bound_const_Td(Td::Real, model::ThermallyDecoupledModel)

Compute the upper bound on ξ = Td/Tsm assuming that Td is held constant.
"""
function ξ_upper_bound_const_Td(Td::Real, model::ThermallyDecoupledModel)
    hdinf = dof_entropy_inf(model)
    hd = dof_entropy(Td, model)
    _ξinf = ξinf(model)
    cbrt(hdinf / hd) * _ξinf
end

"""
    ξ_lower_bound_const_Td(Td::Real, model::ThermallyDecoupledModel)

Compute the lower bound on ξ = Td/Tsm assuming that Td is held constant.
"""
function ξ_lower_bound_const_Td(Td::Real, model::ThermallyDecoupledModel)
    hdinf = dof_entropy_inf(model)
    hd = dof_entropy(Td, model)
    _ξinf = ξinf(model)
    cbrt(hdinf / hd * hsm0 / hsminf) * _ξinf
end

"""
    ξ_lower_bound_const_Tsm(Tsm::Real, m::ThermallyDecoupledModel)

Compute the lower bound on ξ = Td/Tsm assuming that Tsm is held constant.
"""
function ξ_lower_bound_const_Tsm(Tsm::Real, model::ThermallyDecoupledModel)
    hsm = sm_dof_entropy(Tsm)
    hdinf = dof_entropy_inf(model)
    sumg = sum_internal_dof(model)
    _ξinf = ξinf(model)
    cbrt(hsm * hdinf / sumg / hsminf) * _ξinf
end

"""
    ξ_upper_bound_const_Tsm(Tsm::Real, m::ThermallyDecoupledModel)

Compute the upper bound on ξ = Td/Tsm assuming that Tsm is held constant.
"""
function ξ_upper_bound_const_Tsm(Tsm::Real, model::ThermallyDecoupledModel)
    ml = mass_lightest(Tsm, model)
    gl = dof_lightest(Tsm, model)

    hsm = sm_dof_entropy(Tsm)
    hdinf = dof_entropy_inf(model)
    _ξinf = ξinf(model)

    ml == 0 && return cbrt(hsm * hdinf / gl / hsminf) * _ξinf

    xl = ml / Tsm
    pl_arg_num = (45 * gl * hsminf * xl^3)^2
    pl_arg_den = (4 * hdinf * hsm * _ξinf)^2 * π^7
    2xl / product_log(pl_arg_num / pl_arg_den)
end

"""
    compute_ξ_const_Td(Td::Real, m::ThermallyDecoupledModel)

Compute the ratio ξ = Td/Tsm assuming that Td is held constant.
"""
function compute_ξ_const_Td(Td::Real, model::ThermallyDecoupledModel)
    lb = ξ_lower_bound_const_Td(Td, model)
    ub = ξ_upper_bound_const_Td(Td, model)
    # Compute ξ
    hd = dof_entropy(Td, model)
    hdinf = dof_entropy_inf(model)
    _ξinf = ξinf(model)

    f(ξ) = hd * ξ^3 - sm_dof_entropy(Td / ξ) * hdinf * _ξinf^3 / hsminf
    find_zero(f, (0.8lb, 1.2ub), Bisection())
end

"""
    compute_ξ_const_Tsm(Tsm::Real, m::ThermallyDecoupledModel)

Compute the ratio ξ = Td/Tsm assuming that Tsm is held constant.
"""
function compute_ξ_const_Tsm(Tsm::Real, m::ThermallyDecoupledModel)
    ub = ξ_upper_bound_const_Tsm(Tsm, m)
    lb = ξ_lower_bound_const_Tsm(Tsm, m)

    hsm = sm_dof_entropy(Tsm)
    hdinf = dof_entropy_inf(m)
    _ξinf = ξinf(m)

    f(ξ) = dof_entropy(ξ * Tsm, m) * ξ^3 - hsm * hdinf * _ξinf^3 / hsminf
    try
        find_zero(f, (lb * 0.8, ub * 1.2), Bisection())
    catch e
        println("lb = $lb")
        println("ub = $ub")
        println("Tsm = $Tsm")
        throw(e)
    end
end
