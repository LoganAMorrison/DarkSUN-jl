using Interpolations
using DelimitedFiles

data = readdlm(string(@__DIR__) * "/data/smdof.csv", ',', skipstart = 1)
_temperatures = data[:, 1];
_gstars = data[:, 2];
_heffs = data[:, 3];
_geffs = data[:, 4];

"Interpolating function for √gstar"
_gstar_interp = LinearInterpolation(
    _temperatures,
    _gstars,
    extrapolation_bc = Flat(),
)

"Interpolating function for d.o.f. stored in entropy"
_heff_interp = LinearInterpolation(
    _temperatures,
    _heffs,
    extrapolation_bc = Flat(),
)

"Interpolating function for d.o.f. stored in energy"
_geff_interp = LinearInterpolation(
    _temperatures,
    _geffs,
    extrapolation_bc = Flat(),
)

"""
    sm_dof_energy(T::Real)

d.o.f. stored in energy of the sm at temperature `T`
"""
sm_dof_energy(T::Real) = _geff_interp(T);

"""
    sm_dof_entropy(T::Real)

d.o.f. stored in entropy of the sm at temperature `T`.
"""
sm_dof_entropy(T::Real) = _heff_interp(T);

"""
    sqrt_gstar(T::Real)

Square root of gstar of the sm at temperature `T`.
"""
sm_sqrt_gstar(T::Real) = _gstar_interp(T);


"""
    sm_energy_density(T::Real)

Energy density stored in the sm at temperature `T`.
"""
sm_energy_density(T::Real) = π^2 / 30.0 * sm_dof_energy(T) * T^4

"""
    sm_entropy_density(T::Real)

Entropy density stored in the sm at temperature `T`.
"""
sm_entropy_density(T::Real) = 2 * π^2 / 45.0 * sm_dof_entropy(T) * T^3
