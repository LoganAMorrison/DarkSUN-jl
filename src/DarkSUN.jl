module DarkSUN

using Roots
using ForwardDiff
using Interpolations
using SpecialFunctions
using QuadGK
using DelimitedFiles

include("constants.jl")
export M_PLANK
export RHO_CRIT
export S_TODAY
export OMEGA_H2_CDM
export T_CMB

include("utils.jl")
export besselk0
export besselk1
export besselk2
export besselk3
export besselkn
export product_log
export @horner

include("thermodynamic_particles.jl")
export ThermodynamicParticle
export Fermion
export Boson
export dof_energy
export dof_entropy
export neq
export energy_density
export pressure_density
export entropy_density

include("standard_model.jl")
export sm_dof_energy
export sm_dof_entropy
export sm_sqrt_gstar
export sm_energy_density
export sm_entropy_density

include("thermally_decoupled_model.jl")
export ThermallyDecoupledModel
export dof_entropy_inf
export sum_internal_dof
export dof_entropy
export ξinf
export dof_lightest
export mass_lightest
export ξ_upper_bound_const_Td
export ξ_lower_bound_const_Td
export ξ_lower_bound_const_Tsm
export ξ_upper_bound_const_Tsm
export compute_ξ_const_Td
export compute_ξ_const_Tsm

include("model.jl")
export DarkSUNModel
export compute_ξ

include("thermal_cs.jl")
export thermal_cs_ηη_ηηηη
export thermal_cs_ηηηη_ηη
export thermal_cs_ηη_ΔΔ

"""
include("boltzmann.jl")
export DarkSUNBoltzmann
export boltzmann!
export jacobian!

include("semi_analytical.jl")
"""

end # module
