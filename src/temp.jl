mutable struct DarkSUNModel
    # Parameters
    Λ::Float64
    N::Float64
    L1::Float64
    c::Float64
    μη::Float64
    μΔ::Float64
    ξinf::Float64
    η::ThermodynamicBoson
    Δ::ThermodynamicFermion
    has_dp::Bool
    # freeze out
    has_frozen::Bool
    ξfo::Float64
    Tsmfo::Float64
    # Initializations
    x_init::Float64
    x_final::Float64
    Td_init::Float64
    Tsm_init::Float64
    w_init::Array{Float64, 1}
    # Current values
    ξcurrent::Float64
    # thermal cross-section ηηηη→ηη interpolation function
    tcs_interp
end

function DarkSUNModel(Λ::Float64, N::Float64, L1::Float64, c::Float64, μη::Float64,
                      μΔ::Float64, ξinf::Float64, has_dp::Bool)
    η = ThermodynamicBoson(μη * Λ / √N, 1.0)
    Δ = ThermodynamicFermion(μΔ * Λ * N, N + 1)
    # Compute initial variables
    Td0::Float64 = η.mass
    hdinf::Float64 = dark_dof_entropy_inf(N, has_dp)
    hd::Float64 = dark_dof_entropy(Td0, η, Δ, has_dp)
    ξ0::Float64 = compute_ξ_const_Td(Td0, hd, hdinf, ξinf)
    Tsm0::Float64 = Td0 / ξ0
    xinit::Float64 = η.mass / Tsm0
    winit = [log(η.neq(Td0) / sm_entropy_density(Tsm0))]
    xfinal = η.mass / T_CMB
    # Construct function for thermal cs of 4η→2η
    xs = range(xinit/10, stop=150, length=500)
    tcs_interp = generate_tcs_interp_4η_2η(xs, N / μη)

    return DarkSUNModel(Λ, N, L1, c, μη, μΔ, ξinf, η, Δ, has_dp, false,
                        NaN, NaN, xinit, xfinal, Td0, Tsm0, winit, ξ0, tcs_interp)
end

@inline function dark_dof_entropy_inf(N::Float64, has_dp::Bool)
    7.0 / 2.0 * N + 2N^2 - 2.0 + (has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy_inf(model::DarkSUNModel)
    dark_dof_entropy_inf(model.N, model.has_dp)
end

@inline function sum_internal_dark_dof(m::DarkSUNModel)
    m.η.g + 7.0 / 8.0 * m.Δ.g + (m.has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy(Td::Float64, η::ThermodynamicBoson, Δ::ThermodynamicFermion, has_dp::Bool)
    η.dof_entropy(Td) + Δ.dof_entropy(Td) + (has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_entropy(Td::U, m::DarkSUNModel) where U<:Real
    m.η.dof_entropy(Td) + m.Δ.dof_entropy(Td) + (m.has_dp ? 2.0 : 0.0)
end

@inline function dark_dof_energy(Td::U, m::DarkSUNModel) where U<:Real
    m.η.dof_energy(Td) + m.Δ.dof_energy(Td) + (m.has_dp ? 2.0 : 0.0)
end

function thermal_cs_ηη_ηηηη(Td::U, model::DarkSUNModel) where U<:Real
    fη::Float64 = sqrt(model.N) * model.Λ / (4π)
    pf::Float64 = (model.L1^2 * model.η.mass^7 / (fη * model.Λ)^4)^2
    pf * model.tcs_interp(model.η.mass / Td)
end

"""
    thermal_cs_ηηηη_ηη(Td::U, model::DarkSUNModel) where U<:Real

Compute the thermal cross-section for 4η → 2η.

This cross section is computed from the 2η → 4η using:
    ⟨σ(2→4)v⟩ × neq² = ⟨σ(4→2)v⟩ × neq⁴
"""
function thermal_cs_ηηηη_ηη(Td::U, model::DarkSUNModel) where U<:Real
    cs::U = thermal_cs_ηη_ηηηη(Td, model) / model.η.neq(Td)^2
    isfinite(cs) ? cs : 0.0
end

"""
    compute_ξ(Tsm::U, model::DarkSUNModel)

Compute the ratio of dark to SM temperature given a value of the SM
temperature `Tsm`
"""
function compute_ξ(Tsm::U, model::DarkSUNModel) where U<:Real
    darkh(Td::U) where U<:Real = dark_dof_entropy(Td, model)
    hdinf::Float64 = dark_dof_entropy_inf(model)
    sumg::Float64 = sum_internal_dark_dof(model)
    gl::Float64 = model.has_dp ? 2.0 : model.η.g
    ml::Float64 = model.η.mass
    ξinf::Float64 = model.ξinf
    if model.has_dp
        return compute_ξ_const_Tsm(Tsm, darkh, hdinf, sumg, gl, ξinf)
        #return compute_ξ_const_Tsm(Tsm, model.ξcurrent, darkh, hdinf, ξinf)
    else
        if model.has_frozen
            # If the particle freezes out when it it relativistic, ξ=ξfo
            if model.Tsmfo * model.ξfo > model.η.mass
                return model.ξfo
            # If the particle freezes out when it it non-relativistic,
            # ξ is redshifted
            else
                return model.ξfo * Tsm / model.Tsmfo
            end
        else # no dark photon and hasn't frozen out
            return compute_ξ_const_Tsm(Tsm, darkh, hdinf, sumg, gl, ml, ξinf)
            #return compute_ξ_const_Tsm(Tsm, model.ξcurrent, darkh, hdinf, ξinf)
        end
    end
end
