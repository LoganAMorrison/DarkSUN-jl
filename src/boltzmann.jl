using DarkSUN
using DifferentialEquations
using ODEInterfaceDiffEq
using Interpolations
using Plots

mutable struct DarkSUNBoltzmann
    "Dark SU(N) model"
    model::DarkSUNModel
    "initial value of x = mη / Tsm"
    x_init::Float64
    "final value of x = mη / Tsm"
    x_final::Float64
    "initial value of W = log(η.neq(Td) / sm_entropy_density(Tsm))"
    w_init::Array{Float64, 1}
    "freeze out value of ξ"
    ξfo::Float64
    "freeze out value of Tsm"
    Tsmfo::Float64
    "thermal cs ηηηη→ηη"
    tcs_ηη_ηηηη
end

function DarkSUNBoltzmann(
    Λ::Float64,
    N::Int,
    L1::Float64,
    c::Float64,
    μη::Float64,
    μΔ::Float64,
    ξinf::Float64,
    has_dp::Bool
)
    model = DarkSUNModel(Λ, N, L1, c, μη, μΔ, ξinf, has_dp)
    Td_init = Λ / 2
    ξ_init = compute_ξ_const_Td(Td_init, model)
    Tsm_init = Td_init / ξ_init
    x_init = model.η.mass / Tsm_init
    x_final = model.η.mass / T_CMB
    w_init = [log(neq(Td_init, model.η) / sm_entropy_density(Tsm_init))]

    xs = 10 .^ LinRange(log10(model.η.mass / Td_init), log10(150.0), 100);
    log10tcs = [log10(thermal_cs_ηη_ηηηη(model.η.mass / x, model)) for x in xs]
    tcs_ηη_ηηηη = LinearInterpolation(log10.(xs), log10tcs, extrapolation_bc=Flat())


    DarkSUNBoltzmann(model, x_init, x_final, w_init, NaN, NaN, tcs_ηη_ηηηη)
end

function compute_ξ(Tsm::Real, boltz::DarkSUNBoltzmann)
    if isfinite(boltz.ξfo) && Tsm < boltz.Tsmfo
        boltz.model.γ.g != 0 && return compute_ξ_const_Tsm(Tsm, boltz.model)
        boltz.Tsmfo * boltz.ξfo > boltz.model.η.mass && return boltz.ξfo
        boltz.ξfo * Tsm / boltz.Tsmfo
    end
    compute_ξ_const_Tsm(Tsm, boltz.model)
end

"""
    boltzmann!(dw, w, model, logx)

Compute the LHS of the Boltzmann equation.
"""
function boltzmann!(dw, w, boltz::DarkSUNBoltzmann, logx)
    T = boltz.model.η.mass * exp(-logx)
    ξ = compute_ξ(T, boltz)
    Td = T * ξ
    s = sm_entropy_density(T)
    # Determine if we have frozen out of equilibrium
    weqη = log(neq(Td, boltz.model.η) / s)
    if w[1] - weqη > 0.4 && isnan(boltz.ξfo)
        boltz.ξfo = (typeof(ξ) == Float64) ? ξ : ξ.value
        boltz.Tsmfo = (typeof(T) == Float64) ? T : T.value
    end
    σηη = thermal_cs_ηηηη_ηη(Td, boltz.model)
    # Compute overall prefactor
    _dark_dof_energy = dof_energy(Td, boltz.model)
    _sm_dof_energy = sm_dof_energy(T)
    _dof_energy_eff = _sm_dof_energy + _dark_dof_energy * ξ^4
    _sqrt_gstar = sm_sqrt_gstar(T) * sqrt(_sm_dof_energy / _dof_energy_eff)
    pf = -sqrt(π/45) * M_PLANK * _sqrt_gstar * T * s^2
    # η term
    dw[1] = pf * σηη * exp(w[1]) * (exp(2w[1]) - exp(2weqη))
end

"""
    jacobian!(dw, w, model, logx)

Compute the Jacobian of the LHS of the Boltzmann equation.
"""
function jacobian!(J, w, boltz::DarkSUNBoltzmann, logx)
    T = boltz.model.η.mass * exp(-logx)
    ξ = compute_ξ(T, boltz)
    Td = T * ξ
    # Determine if we have frozen out of equilibrium
    s = sm_entropy_density(T)
    weqη = log(neq(Td, boltz.model.η) / s)
    σηη = thermal_cs_ηηηη_ηη(Td, boltz.model)
    # Compute overall prefactor
    _dark_dof_energy = dof_energy(Td, boltz.model)
    _sm_dof_energy = sm_dof_energy(T)
    _dof_energy_eff = _sm_dof_energy + _dark_dof_energy * ξ^4
    _sqrt_gstar = sm_sqrt_gstar(T) * sqrt(_sm_dof_energy / _dof_energy_eff)
    pf = -sqrt(π/45) * M_PLANK * _sqrt_gstar * T * s^2
    J[1, 1] = pf * σηη * exp(w[1]) * (3exp(2w[1]) - exp(2weqη))
end

model = DarkSUNBoltzmann(1e0, 20, 1.0, 1.0, 1.0, 1.0, 1e-1, false)
fj = ODEFunction(boltzmann!; jac=jacobian!)
prob = ODEProblem(fj, model.w_init, (log(model.x_init), log(model.x_final)), model)

sol = solve(prob, radau5(), abstol=1e-5, reltol=1e-7)

plot(sol)
