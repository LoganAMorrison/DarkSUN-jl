using Roots
using ForwardDiff

const _hsminf = _heffs[end]
const _hsm0 = _heffs[1]


"""
    ξ_upper_bound_const_Td(hd, hdinf, ξinf)

Compute the upper bound on ξ = Td/Tsm assuming that Td is held constant.

# Arguments
- `hd::Float64`: dark d.o.f. in entropy at Td
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function ξ_upper_bound_const_Td(hd::Float64, hdinf::Float64, ξinf::Float64)
    cbrt(hdinf / hd) * ξinf
end

"""
    ξ_lower_bound_const_Td(hd, hdinf, ξinf)

Compute the lower bound on ξ = Td/Tsm assuming that Td is held constant.

# Arguments
- `hd::Float64`: dark d.o.f. in entropy at Td
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function ξ_lower_bound_const_Td(hd::Float64, hdinf::Float64, ξinf::Float64)
    cbrt(hdinf / hd * _hsm0 / _hsminf) * ξinf
end

"""
    ξ_lower_bound_const_Tsm(Tsm, hdinf, sumg, ξinf)

Compute the lower bound on ξ = Td/Tsm assuming that Tsm is held constant.

# Arguments
- `Tsm::Float64`: temperature of the SM
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `sumg::Float64`: sum of the internal d.o.f. of dark sector
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function ξ_lower_bound_const_Tsm(
    Tsm::U,
    hdinf::Float64,
    sumg::Float64,
    ξinf::Float64,
) where {U<:Real}
    cbrt(sm_dof_entropy(Tsm) * hdinf / sumg / _hsminf) * ξinf
end

"""
    ξ_upper_bound_const_Tsm(Tsm, hdinf, gl, ξinf)

Compute the upper bound on ξ = Td/Tsm assuming that Tsm is held constant.

# Arguments
- `Tsm::Float64`: temperature of the SM
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `gl::Float64`: sum of internal d.o.f. of massless dark particles in thermal eq.
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function ξ_upper_bound_const_Tsm(
    Tsm::U,
    hdinf::Float64,
    gl::Float64,
    ξinf::Float64,
) where {U<:Real}
    cbrt(sm_dof_entropy(Tsm) * hdinf / gl / _hsminf) * ξinf
end

"""
    ξ_upper_bound_const_Tsm(Tsm, hdinf, gl, ml, ξinf)

Compute the upper bound on ξ = Td/Tsm assuming that Tsm is held constant.

# Arguments
- `Tsm::Float64`: temperature of the SM
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `gl::Float64`: internal d.o.f. of lightest dark particle in thermal eq.
- `ml::Float64`: mass of the lightest dark particle in thermal equilibrium
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function ξ_upper_bound_const_Tsm(
    Tsm::U,
    hdinf::Float64,
    gl::Float64,
    ml::Float64,
    ξinf::Float64,
) where {U<:Real}
    xl::U = ml / Tsm
    hsm::U = sm_dof_entropy(Tsm)
    lw_arg_num::U = (45gl * _hsminf * xl^3)^2
    lw_arg_den::U = (4hdinf * hsm * ξinf)^2 * π^7
    2xl / product_log(lw_arg_num / lw_arg_den)
end

"""
    compute_ξ_const_Td(Td, hd, hdinf, ξinf)

Compute the ratio ξ = Td/Tsm assuming that Td is held constant.

# Arguments
- `Td::Float64`: temperature of the dark sector
- `hd::Float64`: dark d.o.f. in entropy at Td
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function compute_ξ_const_Td(
    Td::Float64,
    hd::Float64,
    hdinf::Float64,
    ξinf::Float64,
)
    lb::Float64 = ξ_lower_bound_const_Td(hd, hdinf, ξinf)
    ub::Float64 = ξ_upper_bound_const_Td(hd, hdinf, ξinf)
    # Compute ξ
    f(ξ::Float64) = hd * ξ^3 - sm_dof_entropy(Td / ξ) * hdinf * ξinf^3 / _hsminf
    find_zero(f, (0.8lb, 1.2ub), Bisection())
end

"""
    compute_ξ_const_Tsm(hd, Tsm, hdinf, sumg, gl, ξinf)

Compute the ratio ξ = Td/Tsm assuming that Tsm is held constant.

# Arguments
- `Tsm::Float64`: temperature of the SM
- `hd::Function`: function to compute d.o.f. in entropy of dark sector
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `sumg::Float64`: sum of all interal d.o.f. of dark particles in equilibrium
- `gl::Float64`: sum of interal d.o.f. of massles dark particles
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function compute_ξ_const_Tsm(
    Tsm::U,
    hd::Function,
    hdinf::Float64,
    sumg::Float64,
    gl::Float64,
    ξinf::Float64,
) where {U<:Real}
    ub::U = ξ_upper_bound_const_Tsm(Tsm, hdinf, gl, ξinf)
    lb::U = ξ_upper_bound_const_Tsm(Tsm, hdinf, sumg, ξinf)
    hsm::U = sm_dof_entropy(Tsm)
    f(ξ::U) = hd(ξ * Tsm) * ξ^3 - hsm * hdinf * ξinf^3 / _hsminf
    return find_zero(f, (lb * 0.8, ub * 1.2), Bisection())
end

"""
    compute_ξ_const_Tsm(hd, Tsm, hdinf, sumg, gl, ξinf)

Compute the ratio ξ = Td/Tsm assuming that Tsm is held constant.

# Arguments
- `Tsm::Float64`: temperature of the SM
- `hd::Function`: function to compute d.o.f. in entropy of dark sector
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `sumg::Float64`: sum of all interal d.o.f. of dark particles in equilibrium
- `gl::Float64`: sum of interal d.o.f. of massles dark particles
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function compute_ξ_const_Tsm(
    Tsm::U,
    hd::Function,
    hdinf::Float64,
    sumg::Float64,
    gl::Float64,
    ml::Float64,
    ξinf::Float64,
) where {U<:Real}
    ub::U = ξ_upper_bound_const_Tsm(Tsm, hdinf, gl, ml, ξinf)
    lb::U = ξ_upper_bound_const_Tsm(Tsm, hdinf, sumg, ξinf)
    hsm::U = sm_dof_entropy(Tsm)
    f(ξ::U) = hd(ξ * Tsm) * ξ^3 - hsm * hdinf * ξinf^3 / _hsminf
    find_zero(f, (lb * 0.8, ub * 1.2), Bisection())
end

"""
    compute_ξ_const_Tsm(hd, Tsm, hdinf, sumg, gl, ξinf)

Compute the ratio ξ = Td/Tsm assuming that Tsm is held constant given an
initial guess of the value of ξ.

# Arguments
- `Tsm::Float64`: temperature of the SM
- `ξguess::Float64`: initial guess for ξ
- `hd::Function`: function to compute d.o.f. in entropy of dark sector
- `hdinf::Float64`: dark d.o.f. in entropy at ∞
- `ξinf::Float64`: ratio of Td / Tsm at ∞
"""
function compute_ξ_const_Tsm(
    Tsm::U,
    ξguess::Float64,
    hd::Function,
    hdinf::Float64,
    ξinf::Float64,
) where {U<:Real}
    hsm::U = sm_dof_entropy(Tsm)
    f(ξ::W) where {W<:Real} = hd(ξ * Tsm) * ξ^3 - hsm * hdinf * ξinf^3 / _hsminf
    df(ξ::W) where {W<:Real} = ForwardDiff.derivative(f, ξ)
    # Solve for ξ using newton's method
    ξn, ξo = ξguess, Inf
    fn, fo = f(ξn), Inf
    tol::Float64, maxiter::Int = 1e-8, 100
    iter::Int64 = 1
    while iter < maxiter && abs(ξn - ξo) > tol && abs(fn - fo) > tol
        ξ = ξn - f(ξn) / df(ξn)
        ξn, ξo = ξ, ξn
        fn, fo = f(ξn), fn
        iter += 1
    end
    if iter >= maxiter
        error("Did not converge in ", string(maxiter), " steps")
    else
        ξn
    end
end

"""
    compute_ξ(Tsm::U, model::DarkSUNModel)

Compute the ratio of dark to SM temperature given a value of the SM
temperature `Tsm`
"""
function compute_ξ(Tsm::U, model::DarkSUNModel) where {U<:Real}
    darkh(Td::U) where {U<:Real} = dark_dof_entropy(Td, model)
    hdinf::Float64 = dark_dof_entropy_inf(model)
    sumg::Float64 = sum_internal_dark_dof(model)
    # if we don't have a dark photon, use η as lightest particle
    gl::Float64 = model.γ.g == 0 ? model.η.g : model.γ.g
    ξinf::Float64 = model.ξinf
    if model.γ.g == 0.0 # no dark photon
        ml::Float64 = model.η.mass
        return compute_ξ_const_Tsm(Tsm, darkh, hdinf, sumg, gl, ml, ξinf)
    else
        return compute_ξ_const_Tsm(Tsm, darkh, hdinf, sumg, gl, ξinf)
    end
end

"""
    compute_ξ(Tsm::U, model::DarkSUNModel)

Compute the ratio of dark to SM temperature after the η has frozen out at a
value of `ξfo`, a SM temperature `Tsmfo` given the current value of the SM
temperature `Tsm`.

# Arguments
-`Tsm`: current SM temperature
-`ξfo`: value of ξ when η freezes out
-`Tsmfo`: value of SM temperature when η freezes out
-`model`: Dark SU(N) model
"""
function compute_ξ(
    Tsm::U,
    ξfo::Float64,
    Tsmfo::Float64,
    model::DarkSUNModel,
) where {U<:Real}
    if model.γ.g == 0.0 # no dark photon
        if Tsmfo * ξfo > model.η.mass
            # relativistic η => ξ = ξfo
            return ξfo
        else
            # non-relativistic η => ξ redshifted
            return ξfo * Tsm / Tsmfo
        end
    else
        # If we have a dark photon, continue to compute ξ as usual
        return compute_ξ(Tsm, model)
    end
end
