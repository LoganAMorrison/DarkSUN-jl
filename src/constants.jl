const M_PLANK = 1.220910e19 # Plank mass in GeV
const RHO_CRIT = 1.05375e-5 # Critical energy density in units of h^2 GeV / cm^3
const S_TODAY = 2891.2 # Entropy density today in units of cm^-3
const OMEGA_H2_CDM = 0.1198 # Dark matter energy fraction times h^2
const T_CMB = 2.56215e-10 # CMB Temperature in GeV
