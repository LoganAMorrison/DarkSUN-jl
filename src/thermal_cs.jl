"""
    thermal_cs_ηη_ΔΔ(T, N, Λ, c, μΔ, μη)

Compute the thermally averaged cross-section for ηη→ΔΔ.

# Arguments
- `T::Real`: η temperature
- `model`: Dark SU(N)
"""
function thermal_cs_ηη_ΔΔ(T::Real, model::DarkSUNModel)
    N::Int = model.N
    Λ::Float64 = model.Λ
    c::Float64 = model.c
    mη::Float64 = model.η.mass
    mΔ::Float64 = model.Δ.mass
    xη::Float64 = mη / T
    pf::Float64 = exp(-2 * c * N) / (8 * T * Λ^2 * mη^4 * besselk2(xη)^2)
    # Integrand
    f(s) = sqrt(s) * (s - 4 * mη^2) * besselk1(sqrt(s) / T)

    return pf * quadgk(f, 4 * mΔ^2, Inf)[1]
end

"""
    thermal_cross_section_ηη_ηηηη(Td::Real, model)

Compute the thermally average cross section for ηη→ηηηη at a temperature `Td`.
"""
function thermal_cross_section_ηη_ηηηη(Td::Real, model::DarkSUNModel)
    fη::Float64 = sqrt(model.N) * model.Λ / (4π)
    pf::Float64 = (model.L1^2 * model.η.mass^7 / (fη * model.Λ)^4)^2
    pf * scaled_tcs_ηη_ηηηη(model.η.mass / Td, model.N, model.μη)
end

"""
    thermal_cross_section_ηηηη_ηη(Td::Real, model::DarkSUNModel)

Compute the thermally average cross section for ηηηη→ηη at a temperature `Td`.
"""
function thermal_cross_section_ηηηη_ηη(Td::Real, model::DarkSUNModel)
    # use: ⟨σ(2→4)v⟩ × neq² = ⟨σ(4→2)v⟩ × neq⁴
    cs = thermal_cs_ηη_ηηηη(Td, model) / neq(Td, model.η)^2
    isfinite(cs) ? cs : 0.0
end

"Functor for the thermal cross section of ηη→Δ̄Δ"
mutable struct ThermalCrossSectionDelta
    "interpolating function for scaled thermal cross section of ηη→Δ̄Δ"
    interp::Function
    "prefactor of thermal cross section of ηη→Δ̄Δ"
    pf::Float64
end

"""
    ThermalCrossSectionDelta(Λ, N, c, μη, μΔ)

Create a thermal cross section functor for ηη→Δ̄Δ given the confinement scale
`Λ`, order of SU(N) `N`, ηη→ΔΔ matrix element suppression constant `c` and
numerical prefactors of the η and Δ masses `μη` and `μΔ`.
"""
function ThermalCrossSectionDelta(
    Λ::Float64,
    N::Int,
    c::Float64,
    μη::Float64,
    μΔ::Float64,
)
    mη::Float64 = μη * Λ / sqrt(N)
    mΔ::Float64 = μΔ * Λ * N
    r = mΔ / mη
    pf::Float64 = exp(-2 * c * N) / (64π * mη^2 * Λ^2)

    σ(z, xη) = (z^2 - 4r^2) * (z^2 - 4) * besselk1(xη * z)
    tcsΔ(xη) = quadgk(z -> σ(z, xη), 2r, Inf)[1] * xη / besselk2(xη)^2

    xmin::Float64 = mη / (Λ / 2)
    xmax::Float64 = 150.0
    logxs = LinRange(log10(xmin), log10(xmax), 100)
    logtcs = [log10(tcsΔ(10^logx)) for logx in logxs]
    logtc_interp = LinearInterpolation(logxs, logtcs, extrapolation_bc = Flat())

    ThermalCrossSectionDelta(x -> 10^logtc_interp(log10(x)), pf)
end

"""
    (tcsΔ::ThermalCrossSectionDelta)(x::Real)

Compute the thermal cross section for ηη→Δ̄Δ from the functor `tcsΔ` given a
scaled decoupled sector temperature `x` = mη / Td.
"""
(tcsΔ::ThermalCrossSectionDelta)(x::Real) = tcsΔ.pf * tcsΔ.interp(x)


"Directory containing data for scaled cross section of ηη→ηηηη"
const _data_dir = string(@__DIR__) * "/../src/data/";
"File with large energy fit parameters of scaled cross-section of ηη→ηηηη"
const _fname_pars = _data_dir * "eta_scaled_cs_large_energy_params_data.csv";
"File with data of scaled cross-section of ηη→ηηηη"
const _fname_cs = _data_dir * "eta_scaled_cs_data.csv";
"fit parameters of scaled cross-section of ηη→ηηηη"
const _fitparams = readdlm(_fnamepars, ','; skipstart = 1)
"data of scaled cross-section of ηη→ηηηη"
const _data = readdlm(_fnamecs, ',')
"large energy fit slope of scaled cross-section of ηη→ηηηη"
const _slope = _fitparams[1]
"large energy fit intercept of scaled cross-section of ηη→ηηηη"
const _intercept = _fitparams[2]
"scaled center-of-mass energies"
const _zs = _data[:, 1]
"scaled cross-sections"
const _cs = _data[:, 2]
"interpolating function of scaled cross-section of ηη→ηηηη"
const _cs_interp = LinearInterpolation(_zs, _cs, extrapolation_bc = Flat())

"""
    scaled_cross_section_ηη_ηηηη(z)

Compute the scaled zero-temperature cross-section for ηη→ηηηη for a scaled
center of mass energy `z` = cme / mη.
"""
function scaled_cross_section_ηη_ηηηη(z::Float64)
    z < zs[1] && return 0.0
    z > zs[end] && return z^slope * 10^intercept
    return interp(z)
end

"""
    scaled_tcs_ηη_ηηηη(x::Real, N::Int, μη::Float64)

Compute the thermally average cross section for ηη→ηηηη at a temperature `Td`.
"""
function scaled_tcs_ηη_ηηηη(x::Real, N::Int, μη::Float64)
    integrand(z::Float64) =
        z^2 * (z^2 - 4) * besselk1(x * z) * scaled_cross_section_ηη_ηηηη(z)
    quadgk(integrand, 4, N / μη)[1] * x / (4 * besselk2(x)^2)
end

"Functor for the thermal cross section of ηη→ηηηη"
mutable struct ThermalCrossSectionEta
    "interpolating function for scaled thermal cross section of ηη→ηηηη"
    interp::Function
    "prefactor of thermal cross section of ηη→ηηηη"
    pf::Float64
end

"""
    ThermalCrossSectionEta(Λ, N, L1, μη)

Create a thermal cross section functor for ηη→ηηηη given the confinement scale
`Λ`, order of SU(N) `N`, ηη→ηη low-energy constant `L1` and numerical
prefactor of the η mass `μη`.
"""
function ThermalCrossSectionEta(Λ::Float64, N::Int, L1::Float64, μη::Float64)
    mη::Float64 = μη * Λ / sqrt(N)
    xmin::Float64 = mη / (Λ / 2)
    xmax::Float64 = 150.0
    logxs = LinRange(log10(xmin), log10(xmax), 100)
    logtcs = [log10(scaled_tcs_ηη_ηηηη(10^logx, N, μη)) for logx in logxs]
    logtc_interp = LinearInterpolation(logxs, logtcs, extrapolation_bc = Flat())

    fη::Float64 = sqrt(N) * Λ / (4π)
    pf::Float64 = (L1^2 * mη^7 / (fη * Λ)^4)^2

    ThermalCrossSectionEta(x -> 10^logtc_interp(log10(x)), pf)
end

"""
    (tcsη::ThermalCrossSectionEta)(x::Real)

Compute the thermal cross section for ηη→ηηηη from the functor `tcsη` given a
scaled decoupled sector temperature `x` = mη / Td.
"""
(tcsη::ThermalCrossSectionEta)(x::Real) = tcsη.pf * tcsη.interp(x)
