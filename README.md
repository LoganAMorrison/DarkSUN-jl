# DarkSUN

[![Build Status](https://travis-ci.com/LoganAMorrison/DarkSUN.jl.svg?branch=master)](https://travis-ci.com/LoganAMorrison/DarkSUN.jl)
[![Build Status](https://ci.appveyor.com/api/projects/status/github/LoganAMorrison/DarkSUN.jl?svg=true)](https://ci.appveyor.com/project/LoganAMorrison/DarkSUN-jl)
[![Codecov](https://codecov.io/gh/LoganAMorrison/DarkSUN.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/LoganAMorrison/DarkSUN.jl)
[![Coveralls](https://coveralls.io/repos/github/LoganAMorrison/DarkSUN.jl/badge.svg?branch=master)](https://coveralls.io/github/LoganAMorrison/DarkSUN.jl?branch=master)
[![Build Status](https://api.cirrus-ci.com/github/LoganAMorrison/DarkSUN.jl.svg)](https://cirrus-ci.com/github/LoganAMorrison/DarkSUN.jl)

Project for studying dark matter candidates from an SU(N) Yang-Mills theory
with a single quark flavor.
