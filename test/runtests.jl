using DarkSUN
using Test
using QuadGK
using SpecialFunctions
using ForwardDiff
using LambertW

include("test_utils.jl")

@testset "DarkSUN.jl" begin
    @testset "Thermal Functions" begin
        @testset "number density" begin
            # nbar test
            boson = Boson(1.0, 1.0)
            fermion = Fermion(1.0, 1.0)
            Ts = 10 .^ (range(-2, stop = 2, length = 10))
            for T in Ts
                exactf = n_exact(T, fermion)
                exactb = n_exact(T, boson)
                approxf = neq(T, fermion)
                approxb = neq(T, boson)
                @test abs(approxf - exactf) / exactf < 0.05
                @test abs(approxb - exactb) / exactb < 0.05
            end
        end
        @testset "number density massless" begin
            boson = Boson(0.0, 1.0)
            fermion = Fermion(0.0, 1.0)
            exactf = 0.0913453711751798
            exactb = 0.121793828233573
            approxf = neq(1.0, fermion)
            approxb = neq(1.0, boson)
            @test abs(approxf - exactf) / exactf < 0.05
            @test abs(approxb - exactb) / exactb < 0.05
        end
        @testset "pressure density" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-2, stop = 2, length = 10))
            for T in Ts
                exactf = p_exact(T, fermion)
                exactb = p_exact(T, boson)
                approxf = pressure_density(T, fermion)
                approxb = pressure_density(T, boson)
                @test abs(approxf - exactf) / exactf < 0.05
                @test abs(approxb - exactb) / exactb < 0.05
            end
        end
        @testset "pressure density massless" begin
            boson = Boson(0.0, 1.0)
            fermion = Fermion(0.0, 1.0)
            exactf = 7 / 8 * π^2 / 30 / 3
            exactb = π^2 / 30 / 3
            approxf = pressure_density(1.0, fermion)
            approxb = pressure_density(1.0, boson)
            @test abs(approxf - exactf) / exactf < 0.05
            @test abs(approxb - exactb) / exactb < 0.05
        end
        @testset "energy density" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-2, stop = 2, length = 10))
            for T in Ts
                exactf = ρ_exact(T, fermion)
                exactb = ρ_exact(T, boson)
                approxf = energy_density(T, fermion)
                approxb = energy_density(T, boson)
                @test abs(approxf - exactf) / exactf < 0.05
                @test abs(approxb - exactb) / exactb < 0.05
            end
        end
        @testset "energy density massless" begin
            boson = Boson(0.0, 1.0)
            fermion = Fermion(0.0, 1.0)
            exactf = 7 / 8 * π^2 / 30
            exactb = π^2 / 30
            approxf = energy_density(1.0, fermion)
            approxb = energy_density(1.0, boson)
            @test abs(approxf - exactf) / exactf < 0.05
            @test abs(approxb - exactb) / exactb < 0.05
        end
        @testset "entropy density" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-2, stop = 2, length = 10))
            for T in Ts
                exactf = (ρ_exact(T, fermion) + p_exact(T, fermion)) / T
                exactb = (ρ_exact(T, boson) + p_exact(T, boson)) / T
                approxf = entropy_density(T, fermion)
                approxb = entropy_density(T, boson)
                @test abs(approxf - exactf) / exactf < 0.2
                @test abs(approxb - exactb) / exactb < 0.2
            end
        end
        @testset "entropy density massless" begin
            boson = Boson(0.0, 1.0)
            fermion = Fermion(0.0, 1.0)
            exactf = 7 / 8 * 2 * π^2 / 45
            exactb = 2 * π^2 / 45
            approxf = entropy_density(1.0, fermion)
            approxb = entropy_density(1.0, boson)
            @test abs(approxf - exactf) / exactf < 0.05
            @test abs(approxb - exactb) / exactb < 0.05
        end
        @testset "dof energy" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-2, stop = 2, length = 10))
            for T in Ts
                exactf = ρ_exact(T, fermion) / T^4 * 30 / π^2
                exactb = ρ_exact(T, boson) / T^4 * 30 / π^2
                approxf = dof_energy(T, fermion)
                approxb = dof_energy(T, boson)
                @test abs(approxf - exactf) / exactf < 0.2
                @test abs(approxb - exactb) / exactb < 0.2
            end
        end
        @testset "dof energy massless" begin
            boson = Boson(0.0, 2.0)
            fermion = Fermion(0.0, 2.0)
            exactf = 7 / 8 * fermion.g
            exactb = boson.g
            approxf = dof_energy(1.0, fermion)
            approxb = dof_energy(1.0, boson)
            @test abs(approxf - exactf) / exactf < 0.05
            @test abs(approxb - exactb) / exactb < 0.05
        end
        @testset "dof entropy" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-2, stop = 2, length = 10))
            for T in Ts
                exactf = (ρ_exact(T, fermion) + p_exact(T, fermion)) / T^4 *
                         45 / (2 * π^2)
                exactb = (ρ_exact(T, fermion) + p_exact(T, fermion)) / T^4 *
                         45 / (2 * π^2)
                approxf = dof_entropy(T, fermion)
                approxb = dof_entropy(T, boson)
                @test abs(approxf - exactf) / exactf < 0.2
                @test abs(approxb - exactb) / exactb < 0.2
            end
        end
        @testset "dof entropy massless" begin
            boson = Boson(0.0, 2.0)
            fermion = Fermion(0.0, 2.0)
            exactf = 7 / 8 * fermion.g
            exactb = boson.g
            approxf = dof_entropy(1.0, fermion)
            approxb = dof_entropy(1.0, boson)
            @test abs(approxf - exactf) / exactf < 0.05
            @test abs(approxb - exactb) / exactb < 0.05
        end
        @testset "number density derivative" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-1, stop = 2, length = 10))
            for T in Ts
                exactf = deriv_n_exact(T, fermion)
                exactb = deriv_n_exact(T, boson)
                approxf = ForwardDiff.derivative(T -> neq(T, fermion), T)
                approxb = ForwardDiff.derivative(T -> neq(T, boson), T)
                @test abs(approxf - exactf) / exactf < 0.2
                @test abs(approxb - exactb) / exactb < 0.2
            end
        end
        @testset "energy density derivative" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-1, stop = 2, length = 10))
            for T in Ts
                exactf = deriv_ρ_exact(T, fermion)
                exactb = deriv_ρ_exact(T, boson)
                approxf = ForwardDiff.derivative(
                    T -> energy_density(T, fermion),
                    T,
                )
                approxb = ForwardDiff.derivative(
                    T -> energy_density(T, boson),
                    T,
                )
                @test abs(approxf - exactf) / exactf < 0.05
                @test abs(approxb - exactb) / exactb < 0.05
            end
        end
        @testset "pressure density derivative" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-1, stop = 2, length = 10))
            for T in Ts
                exactf = deriv_p_exact(T, fermion)
                exactb = deriv_p_exact(T, boson)
                approxf = ForwardDiff.derivative(
                    T -> pressure_density(T, fermion),
                    T,
                )
                approxb = ForwardDiff.derivative(
                    T -> pressure_density(T, boson),
                    T,
                )
                @test abs(approxf - exactf) / exactf < 0.05
                @test abs(approxb - exactb) / exactb < 0.05
            end
        end
        @testset "dof energy derivative" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-1, stop = 1, length = 10))
            for T in Ts
                exactf = deriv_dof_energy_exact(T, fermion)
                exactb = deriv_dof_energy_exact(T, boson)
                approxf = ForwardDiff.derivative(T -> dof_energy(T, fermion), T)
                approxb = ForwardDiff.derivative(T -> dof_energy(T, boson), T)
                @test abs(approxf - exactf) / exactf < 0.1
                @test abs(approxb - exactb) / exactb < 0.1
            end
        end
        @testset "dof entropy derivative" begin
            # nbar test
            fermion = Fermion(1.0, 1.0)
            boson = Boson(1.0, 1.0)
            Ts = 10 .^ (range(-1, stop = 1, length = 10))
            for T in Ts
                exactf = deriv_dof_entropy_exact(T, fermion)
                exactb = deriv_dof_entropy_exact(T, boson)
                approxf = ForwardDiff.derivative(
                    T -> dof_entropy(T, fermion),
                    T,
                )
                approxb = ForwardDiff.derivative(T -> dof_entropy(T, boson), T)
                @test abs(approxf - exactf) / exactf < 0.1
                @test abs(approxb - exactb) / exactb < 0.1
            end
        end
    end
    @testset "Standard Model" begin
        @testset "square-root of g-star" begin
            for (i, T) in enumerate(sm_temperatures)
                exact = sm_gstars[i]
                approx = sm_sqrt_gstar(T)
                @test abs(approx - exact) / exact < 0.05
            end
        end
        @testset "d.o.f. stored in entropy" begin
            for (i, T) in enumerate(sm_temperatures)
                exact = sm_heffs[i]
                approx = sm_dof_entropy(T)
                @test abs(approx - exact) / exact < 0.05
            end
        end
        @testset "d.o.f. stored in energy" begin
            for (i, T) in enumerate(sm_temperatures)
                exact = sm_geffs[i]
                approx = sm_dof_energy(T)
                @test abs(approx - exact) / exact < 0.05
            end
        end
    end
    @testset "Utilities" begin
        @testset "horner" begin
            xs = [-2.0, 0.0, 2.0]
            for x in xs
                @test @horner(x, 1.0, 2.0, 3.0) == 1.0 + 2x + 3.0 * x^2
            end
        end
        @testset "product-log" begin
            xs = [0.1, 1.0, 10.0]
            for x in xs
                exact = lambertw(x)
                approx = product_log(x)
                @test abs(approx - exact) / exact < 0.05
            end
        end
        @testset "besselk0" begin
            xs = [0.1, 1.0, 10.0]
            for x in xs
                exact = besselk(0, x)
                approx = besselk0(x)
                @test abs(approx - exact) / exact < 0.05
            end
        end
        @testset "besselk1" begin
            xs = [0.1, 1.0, 10.0]
            for x in xs
                exact = besselk(1, x)
                approx = besselk1(x)
                @test abs(approx - exact) / exact < 0.05
            end
        end
        @testset "besselkn" begin
            ns = [0, 1, 2, 3, 4]
            xs = [0.1, 1.0, 10.0]
            for n in ns, x in xs
                exact = besselk(n, x)
                approx = besselkn(n, x)
                @test abs(approx - exact) / exact < 0.05
            end
        end
    end
    @testset "Computing ξ" begin
        model1 = DarkSUNModel(1.0, 50, 1.0, 1.0, 1.0, 1.0, 1.0, true)
        model2 = DarkSUNModel(1.0, 50, 1.0, 1.0, 1.0, 1.0, 1.0, false)
        @testset "Constant Td" begin
            Td = 10.0
            ξup1 = ξ_upper_bound_const_Td(Td, model1)
            ξup2 = ξ_upper_bound_const_Td(Td, model2)

            ξlb1 = ξ_lower_bound_const_Td(Td, model1)
            ξlb2 = ξ_lower_bound_const_Td(Td, model2)

            ξ1 = compute_ξ_const_Td(Td, model1)
            ξ2 = compute_ξ_const_Td(Td, model2)

            @test ξlb1 < ξ1 < ξup1
            @test ξlb2 < ξ2 < ξup2
        end
        @testset "Constant Tsm" begin
            Tsm = 10.0
            ξup1 = ξ_upper_bound_const_Tsm(Tsm, model1)
            ξup2 = ξ_upper_bound_const_Tsm(Tsm, model2)

            ξlb1 = ξ_lower_bound_const_Tsm(Tsm, model1)
            ξlb2 = ξ_lower_bound_const_Tsm(Tsm, model2)

            ξ1 = compute_ξ_const_Td(Tsm, model1)
            ξ2 = compute_ξ_const_Td(Tsm, model2)

            @test ξlb1 < ξ1 < ξup1
            @test ξlb2 < ξ2 < ξup2
            
            ξ1 = compute_ξ(Tsm, model1)
            ξ2 = compute_ξ(Tsm, model2)

            @test ξlb1 < ξ1 < ξup1
            @test ξlb2 < ξ2 < ξup2
        end
    end
end
