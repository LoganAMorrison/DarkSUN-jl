using QuadGK
using DelimitedFiles
using DarkSUN

"""
    n_exact(T, p)

Compute the exact equilibrium number density for particle `p` with temperature
`T`.
"""
function n_exact(T::Float64, p::Boson)
    integrand(e::Float64) = e * sqrt(e^2 - p.mass^2) / (exp(e / T) - 1)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (2π^2)
end

"""
    n_exact(T, p)

Compute the exact equilibrium number density for particle `p` with temperature
`T`.
"""
function n_exact(T::Float64, p::Fermion)
    integrand(e::Float64) = e * sqrt(e^2 - p.mass^2) / (exp(e / T) + 1)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (2π^2)
end

"""
    ρ_exact(T, p)

Compute the exact equilibrium energy density for particle `p` with temperature
`T`.
"""
function ρ_exact(T::Float64, p::Boson)
    integrand(e::Float64) = e^2 * sqrt(e^2 - p.mass^2) / (exp(e / T) - 1)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (2π^2)
end

"""
    ρ_exact(T, p)

Compute the exact equilibrium energy density for particle `p` with temperature
`T`.
"""
function ρ_exact(T::Float64, p::Fermion)
    integrand(e::Float64) = e^2 * sqrt(e^2 - p.mass^2) / (exp(e / T) + 1)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (2π^2)
end

"""
    p_exact(T, p)

Compute the exact equilibrium pressure density for particle `p` with temperature
`T`.
"""
function p_exact(T::Float64, p::Boson)
    integrand(e::Float64) = (e^2 - p.mass^2)^1.5 / (exp(e / T) - 1)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (6π^2)
end;

"""
    p_exact(T, p)

Compute the exact equilibrium pressure density for particle `p` with temperature
`T`.
"""
function p_exact(T::Float64, p::Fermion)
    integrand(e::Float64) = (e^2 - p.mass^2)^1.5 / (exp(e / T) + 1)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (6π^2)
end;

"""
    deriv_n_exact(T, p)

Compute the exact derivative of equilibrium number density w.r.t. temperature
for particle `p` with temperature `T`.
"""
function deriv_n_exact(T::Float64, p::Boson)
    integrand(e::Float64) = (e^2 * sqrt(e^2-p.mass^2)* csch(e/(2T))^2)/(4T^2)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (2π^2)
end

"""
    deriv_n_exact(T, p)

Compute the exact derivative of equilibrium number density w.r.t. temperature
for particle `p` with temperature `T`.
"""
function deriv_n_exact(T::Float64, p::Fermion)
    integrand(e::Float64) = (e^2 * sqrt(e^2-p.mass^2)* sech(e/(2T))^2)/(4T^2)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (2π^2)
end

"""
    deriv_ρ_exact(T, p)

Compute the exact derivative of equilibrium energy density w.r.t. temperature
for particle `p` with temperature `T`.
"""
function deriv_ρ_exact(T::Float64, p::Boson)
    integrand(e::Float64) = (e^3 * sqrt(e^2-p.mass^2)* csch(e/(2T))^2)/(4T^2)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (2π^2)
end

"""
    deriv_ρ_exact(T, p)

Compute the exact derivative of equilibrium energy density w.r.t. temperature
for particle `p` with temperature `T`.
"""
function deriv_ρ_exact(T::Float64, p::Fermion)
    integrand(e::Float64) = (e^3 * sqrt(e^2-p.mass^2)* sech(e/(2T))^2)/(4T^2)
    return p.g * quadgk(integrand, p.mass, Inf)[1] / (2π^2)
end

"""
    deriv_p_exact(T, p)

Compute the exact derivative of equilibrium pressure density w.r.t. temperature
for particle `p` with temperature `T`.
"""
function deriv_p_exact(T::Float64, p::Boson)
    integrand(e::Float64) = e * (e^2 - p.mass^2)^1.5 * csch(e / (2T))^2 / (4T^2)
    return quadgk(integrand, p.mass, Inf)[1] / (6π^2)
end;

"""
    deriv_p_exact(T, p)

Compute the exact derivative of equilibrium pressure density w.r.t. temperature
for particle `p` with temperature `T`.
"""
function deriv_p_exact(T::Float64, p::Fermion)
    integrand(e::Float64) = e * (e^2 - p.mass^2)^1.5 * sech(e / (2T))^2 / (4T^2)
    return quadgk(integrand, p.mass, Inf)[1] / (6π^2)
end;

"""
    deriv_dof_energy_exact(T, p)

Compute the exact derivative of d.o.f. stored in energy w.r.t. temperature for
particle `p` with temperature `T`.
"""
function deriv_dof_energy_exact(T::Float64, p::Boson)
    x::Float64 = p.mass / T
    integrand(z::Float64) = -x * z^2 / (exp(z) - 1) / sqrt(-x^2 + z^2)
    return quadgk(integrand, x, Inf)[1] * 30 / (2π^4) * (-x^2 / p.mass)
end

"""
    deriv_dof_energy_exact(T, p)

Compute the exact derivative of d.o.f. stored in energy w.r.t. temperature for
particle `p` with temperature `T`.
"""
function deriv_dof_energy_exact(T::Float64, p::Fermion)
    x::Float64 = p.mass / T
    integrand(z::Float64) = -x * z^2 / (exp(z) + 1) / sqrt(-x^2 + z^2)
    return quadgk(integrand, x, Inf)[1] * 30 / (2π^4) * (-x^2 / p.mass)
end

"""
    deriv_dof_entropy_exact(T, p)

Compute the exact derivative of d.o.f. stored in entropy w.r.t. temperature for
particle `p` with temperature `T`.
"""
function deriv_dof_entropy_exact(T::Float64, p::Boson)
    x::Float64 = p.mass / T
    integrand(z::Float64) = (x^3 - 2x * z^2)/((exp(z) - 1) * sqrt(-x^2 + z^2))
    return quadgk(integrand, x, Inf)[1] * 45 / (4π^4)* (-x^2 / p.mass)
end

"""
    deriv_dof_entropy_exact(T, p)

Compute the exact derivative of d.o.f. stored in entropy w.r.t. temperature for
particle `p` with temperature `T`.
"""
function deriv_dof_entropy_exact(T::Float64, p::Fermion)
    x::Float64 = p.mass / T
    integrand(z::Float64) = (x^3 - 2x * z^2)/((exp(z) + 1) * sqrt(-x^2 + z^2))
    return quadgk(integrand, x, Inf)[1] * 45 / (4π^4)* (-x^2 / p.mass)
end

_data = readdlm(string(@__DIR__) * "/../src/data/smdof.csv", ',', skipstart = 1);
sm_temperatures = _data[:, 1];
sm_gstars = _data[:, 2];
sm_heffs = _data[:, 3];
sm_geffs = _data[:, 4];
